# Call Plan  README

## Web Folder
This folder is for web backend. Web is laravel 5.4 with [voyager](https://laravelvoyager.com). API is [Dingo](https://github.com/dingo/api/wiki/Installation). Web backend system is easy to run on [laravel homestead](https://laravel.com/docs/5.4/homestead). Download and install it when you run the development version.

Homestead URL: [http://homestead.test/admin](http://homestead.test/admin)

## Android
Android version 5.0 - 7.x
