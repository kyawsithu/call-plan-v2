package com.ktz.callplan.api;

import com.ktz.callplan.entities.Prescription;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by kyawsithu on 12/2/17.
 */

public interface Prescriptions {
    @GET("/api/prescriptions")
    Call<List<Prescription>> getPrescriptions(@Header("Authorization") String authHeader);
}
