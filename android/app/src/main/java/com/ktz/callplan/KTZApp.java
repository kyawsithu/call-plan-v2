package com.ktz.callplan;
import android.app.Application;
/**
 * Created by kyawsithu on 12/2/17.
 */

public class KTZApp extends Application {
    private String hashKey;

    public String getHashKey() {
        return hashKey;
    }

    public void setHashKey(String hashKey) {
        this.hashKey = hashKey;
    }
}
