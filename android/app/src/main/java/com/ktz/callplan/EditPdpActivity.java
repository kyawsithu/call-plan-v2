package com.ktz.callplan;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ktz.callplan.api.Pdps;
import com.ktz.callplan.api.Perceptions;
import com.ktz.callplan.api.Prescriptions;
import com.ktz.callplan.entities.FullSchedule;
import com.ktz.callplan.entities.Perception;
import com.ktz.callplan.entities.Prescription;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kyawsithu on 12/26/17.
 */

public class EditPdpActivity extends AppCompatActivity {

    private FullSchedule schedule;
    private KTZApp myApp = null;

    @BindView(R.id.product)
    TextView txtProduct;

    @BindView(R.id.doctor)
    TextView txtDoctor;

    @BindView(R.id.perception)
    Spinner spPerception;

    @BindView(R.id.prescription)
    Spinner spPrescription;

    private ProgressDialog progressDialog;

    List<Prescription> prescriptions;
    List<Perception> perceptions;

    Boolean IsLoaded1 = false;
    Boolean IsLoaded2 = false;

    private int perceptionId;
    private int prescriptionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editpdp);
        ButterKnife.bind(this);
        actionBarSetup();

//        spPrescription.setOnItemSelectedListener(this);
//        spPerception.setOnItemSelectedListener(this);

        // Get App variable
        myApp = ((KTZApp)getApplicationContext());

        Bundle data = getIntent().getExtras();
        schedule = (FullSchedule) data.getParcelable("Schedule");

        // Progressbar
        progressDialog = new ProgressDialog(EditPdpActivity.this);
        progressDialog.setMessage("Fetching data..."); // Setting Message
        progressDialog.setTitle("Loading"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        // Populdate textboxes
        txtDoctor.setText(schedule.getDoctor());
        txtProduct.setText(schedule.getProduct());

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Get Potentials
        Prescriptions poClient = retrofit.create(Prescriptions.class);
        Call<List<Prescription>> poCall = poClient.getPrescriptions(authHeader);

        poCall.enqueue(new Callback<List<Prescription>>() {
            @Override
            public void onResponse(Call<List<Prescription>> call, Response<List<Prescription>> response) {
                if (response.isSuccessful()) {
                    prescriptions = response.body();

                    final ArrayList<SpinnerItem> items = new ArrayList<SpinnerItem>();
                    SpinnerItem selectedItem = null;
                    for (Prescription pres: prescriptions) {
                        SpinnerItem item = new SpinnerItem(pres.getName(), Integer.toString(pres.getId()));
                        items.add(item);

                        if (schedule.getPotential().equals(pres.getName())){
                            selectedItem = item;
                        }
                    }

                    CustomAdapter cusAdapt = new CustomAdapter(EditPdpActivity.this, R.layout.spinner_row, items);
                    spPrescription.setAdapter(cusAdapt);
                    spPrescription.setSelection(cusAdapt.getPosition(selectedItem));

                    IsLoaded1 = true;
                    DismissProgressbar();
                } else {
                    Log.e("Failed", "Failed");
                }
            }

            @Override
            public void onFailure(Call<List<Prescription>> call, Throwable t) {
                t.printStackTrace();
                Log.e("Failed", "Failed");
            }
        });

        // Get Perceptoions
        Perceptions prClient =  retrofit.create(Perceptions.class);
        Call<List<Perception>> prCall = prClient.getPerceptions(authHeader);

        prCall.enqueue(new Callback<List<Perception>>() {
            @Override
            public void onResponse(Call<List<Perception>> call, Response<List<Perception>> response) {
                if (response.isSuccessful()) {
                    perceptions = response.body();

                    final ArrayList<SpinnerItem> items = new ArrayList<SpinnerItem>();
                    SpinnerItem selectedItem = null;
                    for (Perception pres: perceptions) {
                        SpinnerItem item = new SpinnerItem(pres.getName(), Integer.toString(pres.getId()));
                        items.add(item);

                        if (schedule.getPerception().equals(pres.getName())){
                            selectedItem = item;
                        }
                    }

                    CustomAdapter cusAdapt = new CustomAdapter(EditPdpActivity.this, R.layout.spinner_row, items);
                    spPerception.setAdapter(cusAdapt);
                    spPerception.setSelection(cusAdapt.getPosition(selectedItem));

                    IsLoaded2 = true;
                    DismissProgressbar();
                } else {
                    Log.e("Failed", "Failed");
                }
            }

            @Override
            public void onFailure(Call<List<Perception>> call, Throwable t) {
                t.printStackTrace();
                Log.e("Failed", "Failed");
            }
        });


    }

    private void DismissProgressbar()
    {
        if (IsLoaded1 && IsLoaded2){
            progressDialog.dismiss();
        }
    }

    /**
     * Sets the Action Bar for new Android versions.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("Edit PDP");
            ab.setSubtitle("Call Details");
            ab.setDisplayHomeAsUpEnabled(true);
            //ab.setHomeButtonEnabled(true);
        }
    }


    @OnClick(R.id.btn_edit)
    public void onClick() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SpinnerItem selectedItem1 = (SpinnerItem) spPrescription.getSelectedItem();
        SpinnerItem selectedItem2 = (SpinnerItem) spPerception.getSelectedItem();
        perceptionId = Integer.parseInt(selectedItem2.key);
        prescriptionId = Integer.parseInt(selectedItem1.key);

        Pdps pClient = retrofit.create(Pdps.class);
        Call<Boolean> call = pClient.pdpEdit(authHeader, schedule.getPdpId(), prescriptionId, perceptionId);

        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(EditPdpActivity.this, "Pdp is successfully edited.", Toast.LENGTH_SHORT).show();
                    backToDetails();
                } else {
                    Log.e("Failed", "Failed");
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                t.printStackTrace();
                Log.e("Failed", "Failed");
            }
        });
    }

    @Override
    public void onBackPressed() {
        backToDetails();
    }

    public void backToDetails() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.finish();
//        intent.putExtra("FullSchedule", schedule);
        startActivity(intent);
    }

    class SpinnerItem {
        public SpinnerItem(String label, String key) {
            this.label = label;
            this.key = key;
        }

        String label;
        String key;
    }

    class CustomAdapter extends ArrayAdapter<SpinnerItem> {

        public CustomAdapter(Context context, int item_layout, List<SpinnerItem> objects) {
            super(context, item_layout, objects);
        }

        @Override //don't override if you don't want the default spinner to be a two line view
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView);
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            return initView(position, convertView);
        }

        private View initView(int position, View convertView) {
            if(convertView == null)
                convertView = View.inflate(getContext(),
                        R.layout.spinner_row,
                        null);
            TextView tvText1 = (TextView)convertView.findViewById(R.id.text1);
            tvText1.setText(getItem(position).label);
            tvText1.setTag(getItem(position).key);
            return convertView;
        }
    }
}
