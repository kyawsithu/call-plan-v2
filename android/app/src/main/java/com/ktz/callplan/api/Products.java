package com.ktz.callplan.api;

import com.ktz.callplan.entities.Pdp;
import com.ktz.callplan.entities.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by kyawsithu on 12/10/17.
 */

public interface Products {
    @GET("/api/products")
    Call<List<Product>> getProducts(@Header("Authorization") String authHeader);

    @GET("/api/pdps")
    Call<List<Pdp>> getPdps(@Header("Authorization") String authHeader, @Query("doctor_id") String doctorId);
}
