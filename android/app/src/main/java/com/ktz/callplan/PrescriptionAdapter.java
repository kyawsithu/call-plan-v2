package com.ktz.callplan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ktz.callplan.entities.Prescription;

import java.util.List;

/**
 * Created by kyawsithu on 12/27/17.
 */
public class PrescriptionAdapter extends BaseAdapter {
    Context context;
    List<Prescription> prescriptions;
    LayoutInflater inflater;

    public PrescriptionAdapter(Context applicationContext, List<Prescription> prescriptions) {
        this.context = applicationContext;
        this.prescriptions = prescriptions;
        inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return prescriptions.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.spinner_row, null);
        TextView txt1 = (TextView) view.findViewById(R.id.text1);
        txt1.setText(prescriptions.get(i).getName());
        return view;
    }
}