package com.ktz.callplan.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by kyawsithu on 12/26/17.
 */

public interface Pdps {

    @FormUrlEncoded
    @POST("/api/pdpEdit")
    Call<Boolean> pdpEdit(@Header("Authorization") String authHeader,
                          @Field("id") int id,
                          @Field("potentialId") int potentialId,
                          @Field("perceptionId") int perceptionId);

}
