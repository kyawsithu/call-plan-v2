package com.ktz.callplan.api;

import com.ktz.callplan.entities.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by kyawsithu on 12/3/17.
 */

public interface Users{
    @GET("/api/user")
    Call<User> getUser(@Header("Authorization") String authHeader, @Query("email") String email);
}
