package com.ktz.callplan.entities;

import android.os.Parcel;
import android.os.Parcelable;

//import com.google.firebase.database.Exclude;
//import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yoon on 1/11/2017.
 */
//@IgnoreExtraProperties
public class CallDetails implements Parcelable{

    public String key;
    public String callpriority;
    public long date;
    public String doctor;
    public String mr;
    public String otherinvestment;
    public String pdp;
    public String perception;
    public String product;
    public String productDoctor;
    public long samplequantity;
    public int status;

    public ArrayList<Doctor> doctors = new ArrayList<>();

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCallpriority() {
        return callpriority;
    }

    public void setCallpriority(String callpriority) {
        this.callpriority = callpriority;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getMr() {
        return mr;
    }

    public void setMr(String mr) {
        this.mr = mr;
    }

    public String getOtherinvestment() {
        return otherinvestment;
    }

    public void setOtherinvestment(String otherinvestment) {
        this.otherinvestment = otherinvestment;
    }

    public String getProductDoctor() {
        return productDoctor;
    }

    public void setProductDoctor(String productDoctor) {
        this.productDoctor = productDoctor;
    }

    public String getPdp() {
        return pdp;
    }

    public void setPdp(String pdp) {
        this.pdp = pdp;
    }

    public String getPerception() {
        return perception;
    }

    public void setPerception(String perception) {
        this.perception = perception;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public long getSamplequantity() {
        return samplequantity;
    }

    public void setSamplequantity(long samplequantity) {
        this.samplequantity = samplequantity;
    }

    public ArrayList<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(ArrayList<Doctor> doctors) {
        this.doctors = doctors;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public CallDetails() {
        // Default constructor required for calls to DataSnapshot.getValue(CallDetails.class)
    }

    public CallDetails(String key, String callpriority, long date, String doctor, String mr, String otherinvestment, String pdp, String perception, String product, String productDoctor, long samplequantity, int status) {
        this.key = key;
        this.callpriority = callpriority;
        this.date = date;
        this.doctor = doctor;
        this.mr = mr;
        this.otherinvestment = otherinvestment;
        this.pdp = pdp;
        this.perception = perception;
        this.product = product;
        this.productDoctor = productDoctor;
        this.samplequantity = samplequantity;
        this.status = status;
    }

//    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("key", key);
        result.put("callpriority", callpriority);
        result.put("date", date);
        result.put("doctor", doctor);
        result.put("mr", mr);
        result.put("otherinvestment", otherinvestment);
        result.put("pdp", pdp);
        result.put("perception", perception);
        result.put("product", product);
        result.put("productDoctor", productDoctor);
        result.put("samplequantity", samplequantity);
        result.put("status", status);
        return result;
    }

    // Parcelling part
    public CallDetails(Parcel in){
//        String[] data = new String[12];
//
//        in.readStringArray(data);
//
//        this.key = data[0];
//        this.callpriority = data[1];
//        this.date = Long.parseLong(data[2]);
//        this.doctor = data[3];
//        this.mr = data[4];
//        this.otherinvestment = data[5];
//        this.pdp = data[6];
//        this.perception = data[7];
//        this.product = data[8];
//        this.productDoctor = data[9];
//        this.samplequantity = Long.parseLong(data[10]);
//        this.status = Integer.parseInt(data[11]);

        //in.readInt();
        this.key = in.readString();
        this.callpriority = in.readString();
        this.date = in.readLong();
        this.doctor = in.readString();
        this.mr = in.readString();
        this.otherinvestment = in.readString();
        this.pdp = in.readString();
        this.perception = in.readString();
        this.product = in.readString();
        this.productDoctor = in.readString();
        this.samplequantity = in.readLong();
        this.status = in.readInt();
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeStringArray(new String[] {
//                this.key,
//                this.callpriority,
//                String.valueOf(this.date),
//                this.doctor,
//                this.mr,
//                this.otherinvestment,
//                this.pdp,
//                this.perception,
//                this.product,
//                this.productDoctor,
//                String.valueOf(this.samplequantity),
//                String.valueOf(this.status)
//        });
        dest.writeString(this.key);
        dest.writeString(this.callpriority);
        dest.writeLong(this.date);
        dest.writeString(this.doctor);
        dest.writeString(this.mr);
        dest.writeString(this.otherinvestment);
        dest.writeString(this.pdp);
        dest.writeString(this.perception);
        dest.writeString(this.product);
        dest.writeString(this.productDoctor);
        dest.writeLong(this.samplequantity);
        dest.writeInt(this.status);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CallDetails createFromParcel(Parcel in) {
            return new CallDetails(in);
        }

        public CallDetails[] newArray(int size) {
            return new CallDetails[size];
        }
    };
}
