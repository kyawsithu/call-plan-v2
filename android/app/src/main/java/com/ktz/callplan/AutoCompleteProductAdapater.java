package com.ktz.callplan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.ktz.callplan.entities.Pdp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yoon on 2/8/2017.
 */

public class AutoCompleteProductAdapater  extends ArrayAdapter<Pdp> {
    private Context context;
    private final List<Pdp> pros;
    private List<Pdp> filteredPros = new ArrayList<>();

    public AutoCompleteProductAdapater(Context context, List<Pdp> pros) {
        super(context, 0, pros);
        this.context = context;
        this.pros = pros;
    }

    @Override
    public int getCount() {
        return filteredPros.size();
    }

    @Override
    public Filter getFilter() {
        return new AutoCompleteProductAdapater.ProductFilter(this, pros);
    }

    @Override
    public Pdp getItem(int position) {
        return filteredPros.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item from filtered list.
        Pdp pro = filteredPros.get(position);

        // Inflate your custom row layout as usual.
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.spinner_row, parent, false);
        }

        if (pro != null){
            TextView tvName = (TextView) convertView.findViewById(R.id.text1);
            tvName.setText(pro.getName() + ", " + pro.getPotential());
        }
        return convertView;
    }

    class ProductFilter extends Filter {

        AutoCompleteProductAdapater adapter;
        List<Pdp> originalList;
        List<Pdp> filteredList;

        public ProductFilter(AutoCompleteProductAdapater adapter, List<Pdp> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = originalList;
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                // Your filtering logic goes in here
                for (final Pdp pro : originalList) {
                    if (pro.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(pro);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredPros.clear();
            adapter.filteredPros.addAll((List) results.values);
            adapter.notifyDataSetChanged();
        }
    }
}
