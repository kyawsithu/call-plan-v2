package com.ktz.callplan;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ktz.callplan.api.CallPriorities;
import com.ktz.callplan.api.Doctors;
import com.ktz.callplan.api.Products;
import com.ktz.callplan.api.Schedules;
import com.ktz.callplan.entities.CallPriority;
import com.ktz.callplan.entities.Doctor;
import com.ktz.callplan.entities.Pdp;
import com.ktz.callplan.entities.ProductDoctor;
import com.ktz.callplan.entities.Schedule;
import com.ktz.callplan.entities.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ktz.callplan.R.id.act_doc;

//import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Yoon on 1/11/2017.
 */

public class CreateCallActivity extends AppCompatActivity {
    private static final String TAG = "CreateCallActivity";

    @BindView(R.id.calldate)
    EditText calldate;

    @BindView(R.id.otherinvestment)
    EditText otherinvestment;

    @BindView(R.id.spinner_callpriority)
    Spinner callpriority;

    @BindView(act_doc)
    AutoCompleteTextView actdoc;

    @BindView(R.id.mainlayout)
    LinearLayout container;

    private DatePickerDialog callDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    private int doctorId;
    private AutoCompleteProductAdapater prAdapter = null;

    private int ctrlId = 10;
    private int ind = 3;

    private KTZApp myApp = null;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_schedule);
        ButterKnife.bind(this);

        myApp = ((KTZApp)getApplicationContext());

        Gson gson = new Gson();
        final SharedPreferences settings = getSharedPreferences(Constant.PREF, MODE_PRIVATE);
        String json = settings.getString(Constant.USER_PREF, "");
        user = gson.fromJson(json, User.class);
        actionBarSetup();

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        calldate.setInputType(InputType.TYPE_NULL);

        Calendar newCalendar = Calendar.getInstance();
        callDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                calldate.setText(dateFormatter.format(newDate.getTime()));
                calldate.setTag(newDate.getTimeInMillis());
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        getDoctorAutoComplete();

        actdoc.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
                Doctor doc = (Doctor) parent.getAdapter().getItem(pos);
                actdoc.setText(doc.getName() + ", " + doc.getDesignation() + ", " + doc.getRegion());
                actdoc.setTag(doc);
                doctorId = doc.getId();
                getProductAutoComplete(doc.getId());
            }
        });

        final AutoCompleteTextView actv = getNewACTV(ctrlId+=1);
        TextInputLayout tl = getNewTInputL(0.92f);
        TextInputLayout tlQty = getNewTInputL();
        LinearLayout ll = getNewLinearL(ctrlId+=1);
        EditText edt = getEDT(ctrlId+=1);
        ImageView iv = getAddBtn(ctrlId+=1);

        tl.addView(actv);
        tlQty.addView(edt);
        ll.addView(tl);
        ll.addView(iv);

        container.addView(ll, ind+=1);
        container.addView(tlQty, ind+=1);

        actv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                ProductDoctor pro = (ProductDoctor) arg0.getAdapter().getItem(arg2);
                actv.setText(pro.getProduct() + ", " + pro.getPdp());
                actv.setTag(pro);
            }
        });

        callPriorityLoading();
        getCallPriority();


    }

    private void callPriorityLoading() {
        final ArrayList<SpinnerItem> drs = new ArrayList<SpinnerItem>();
        SpinnerItem topItem = new SpinnerItem("Call priority is loading...", "");
        drs.add(topItem);
        Spinner cusSpinner = (Spinner) findViewById(R.id.spinner_callpriority);
        CustomAdapter cusAdapt = new CustomAdapter(CreateCallActivity.this, R.layout.spinner_row, drs);
        cusSpinner.setAdapter(cusAdapt);
    }

    private void getCallPriority() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CallPriorities cpClient = retrofit.create(CallPriorities.class);
        Call<List<CallPriority>> call = cpClient.getCallPriority(authHeader);

        call.enqueue(new Callback<List<CallPriority>>() {
            @Override
            public void onResponse(Call<List<CallPriority>> call, Response<List<CallPriority>> response) {
                if (response.isSuccessful()) {
                    List<CallPriority> cps = response.body();
                    Log.e("CreateCallActivity", "success");

                    final ArrayList<SpinnerItem> items = new ArrayList<SpinnerItem>();
                    SpinnerItem topItem = new SpinnerItem("Please select call priority", "");
                    items.add(topItem);

                    for (CallPriority cp: cps) {
                        SpinnerItem item = new SpinnerItem(cp.getName(), Integer.toString(cp.getId()));
                        items.add(item);
                    }
                    Spinner cusSpinner = (Spinner) findViewById(R.id.spinner_callpriority);
                    CustomAdapter cusAdapt = new CustomAdapter(CreateCallActivity.this, R.layout.spinner_row, items);
                    cusSpinner.setAdapter(cusAdapt);
                    cusSpinner.requestFocus();

                } else {
                    Log.e("CreateCallActivity >>", "failed");
                }
            }

            @Override
            public void onFailure(Call<List<CallPriority>> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.calldate)
    public void onClick() {
        callDatePickerDialog.show();
    }

    @OnClick(R.id.btn_create)
    public void CreateClick() {

        boolean valStatus = true;
        int otherInvest = 0;


        // Validate
        SpinnerItem spinnerItem = (SpinnerItem) callpriority.getSelectedItem();
        if (spinnerItem.label.equals("Please select call priority")){
            View selectedView = callpriority.getSelectedView();
            if (selectedView != null && selectedView instanceof TextView) {
                callpriority.requestFocus();
                TextView selectedTextView = (TextView) selectedView;
                selectedTextView.setError("error"); // any name of the error will do
                selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
                selectedTextView.setText(R.string.err_callpriority); // actual error message
                //callpriority.performClick(); // to open the spinner list if error is found.
            }
            valStatus = false;
        }

        if (calldate.getText().toString().isEmpty()){
            calldate.setError(getText(R.string.err_calldate));
            valStatus = false;
        }

        if (!otherinvestment.getText().toString().isEmpty()){
            try {
                otherInvest = Integer.parseInt(otherinvestment.getText().toString());
            } catch (NumberFormatException e) {
                otherInvest = 0;
            }

        }

        Map<String, Object> childUpdates = new HashMap<>();

        // Loop through the Product Textboxes and insert data
        for (int i = 0; i < container.getChildCount(); i++){
            final View child = container.getChildAt(i);
            if (child.getClass() == LinearLayout.class){
                for (int j = 0; j < ((LinearLayout) child).getChildCount(); j++){
                    final View innerChild = ((LinearLayout) child).getChildAt(j);
                    if (innerChild.getClass() == TextInputLayout.class){
                        final AutoCompleteTextView _attv = (AutoCompleteTextView) ((FrameLayout) ((TextInputLayout) innerChild).getChildAt(0)).getChildAt(0);
                        int sampleqtyId = _attv.getId() + 2;
                        final EditText _edtv = (EditText) container.findViewById(sampleqtyId);

                        if (actdoc.getTag() == ""){
                            actdoc.setError(getText(R.string.err_doctor));
                            valStatus = false;
                        }

                        if (_attv.getTag() == ""){
                            _attv.setError(getText(R.string.err_product));
                            valStatus = false;
                        }

                        if (_edtv.getText().toString().isEmpty()){
                            _edtv.setError(getText(R.string.err_qty));
                            valStatus = false;
                        }

                        if (valStatus) {
                            // Get Schedule Date and Format it
                            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                            Date scheduleDate = null;
                            try {
                                scheduleDate = dateFormatter.parse(calldate.getText().toString());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            dateFormatter = new SimpleDateFormat("yyy-MM-dd", Locale.US);

                            // Get Doctor and Pdp from create view
                            Doctor doc = (Doctor) actdoc.getTag();
                            Pdp pdp = (Pdp) _attv.getTag();

                            Schedule s = new Schedule();

                            s.setPdpId(pdp.getPdpId());
                            s.setMrId(user.getId());
                            s.setOtherInvestment(otherInvest);
                            s.setSampleQty(Integer.parseInt(_edtv.getText().toString()));
                            s.setScheduleDate(dateFormatter.format(scheduleDate).toString());
                            s.setCallPriorityId(Integer.parseInt(spinnerItem.key));
                            s.setLatlong(0);
                            // s.setLatlong(String.valueOf(latitude) + "," + String.valueOf(longitude));
                            s.setStatus(0);

                            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
                            OkHttpClient client = new OkHttpClient.Builder()
                                    .addInterceptor(logging)
                                    .build();

                            String authHeader = myApp.getHashKey();
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(Constant.API_URL)
                                    .client(client)
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();

                            Schedules shClient = retrofit.create(Schedules.class);
                            Call<Integer> call = shClient.createSchedule(authHeader, s);
                            call.enqueue(new Callback<Integer>() {
                                @Override
                                public void onResponse(Call<Integer> call, Response<Integer> response) {
                                    if (response.isSuccessful()) {
                                        Log.i(TAG, "Schedule successfully created!!");
                                    }
                                }

                                @Override
                                public void onFailure(Call<Integer> call, Throwable t) {
                                    Log.i(TAG, "Failed new record create");
                                }
                            });
                        }
                    }
                }
            }
        }

        if (valStatus){
            // Reset form
            Activity mActivity = CreateCallActivity.this;
            if (Build.VERSION.SDK_INT >= 11) {
                mActivity.recreate();
            } else {
                mActivity.finish();
                mActivity.startActivity(mActivity.getIntent());
            }

            callpriority.setSelection(0);
            calldate.setText("");
            calldate.setTag("");
            actdoc.setText("");
            otherinvestment.setText("");

            calldate.setError(null);
            calldate.setError(null);
            actdoc.setError(null);
            otherinvestment.setError(null);

            for (int i = 0; i < container.getChildCount(); i++){
                final View child = container.getChildAt(i);
                if (child.getClass() == LinearLayout.class){
                    for (int j = 0; j < ((LinearLayout) child).getChildCount(); j++){
                        final View innerChild = ((LinearLayout) child).getChildAt(j);
                        if (innerChild.getClass() == TextInputLayout.class) {
                            final AutoCompleteTextView _attv = (AutoCompleteTextView) ((FrameLayout) ((TextInputLayout) innerChild).getChildAt(0)).getChildAt(0);
                            int sampleqtyId = _attv.getId() + 2;
                            final EditText _edtv = (EditText) container.findViewById(sampleqtyId);

                            _attv.setText("");
                            _attv.setTag("");
                            _edtv.setText("");

                            _attv.setError(null);
                            _edtv.setError(null);
                        }
                    }
                }
            }
            Toast.makeText(this.getApplicationContext(), "New call is created successfully.", Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Sets the Action Bar for new Android versions.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("");
            ab.setSubtitle("Create new schedule");
            ab.setDisplayHomeAsUpEnabled(true);
            //ab.setHomeButtonEnabled(true);
        }
    }

    private void getDoctorAutoComplete() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Doctors drClient = retrofit.create(Doctors.class);
        Call<List<Doctor>> call = drClient.getDoctors(authHeader);

        call.enqueue(new Callback<List<Doctor>>() {
            @Override
            public void onResponse(Call<List<Doctor>> call, Response<List<Doctor>> response) {
                if (response.isSuccessful()) {
                    List<Doctor> doctors = response.body();

                    AutoCompleteDoctorAdapter drAdapter = new AutoCompleteDoctorAdapter(getApplicationContext(), doctors);
                    actdoc.setAdapter(drAdapter);
                    actdoc.setThreshold(1);
                }
            }

            @Override
            public void onFailure(Call<List<Doctor>> call, Throwable t) {

            }
        });
    }

    private void getProductAutoComplete(int id) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Products prClient = retrofit.create(Products.class);
        Call<List<Pdp>> call = prClient.getPdps(authHeader, String.valueOf(doctorId));

        call.enqueue(new Callback<List<Pdp>>() {
            @Override
            public void onResponse(Call<List<Pdp>> call, Response<List<Pdp>> response) {
                if (response.isSuccessful()) {
                    List<Pdp> pdps = response.body();
                    prAdapter = new AutoCompleteProductAdapater(getApplicationContext(), pdps);

                    for (int i = 0; i < container.getChildCount(); i++) {
                        final View child = container.getChildAt(i);
                        if (child.getClass() == LinearLayout.class) {
                            for (int j = 0; j < ((LinearLayout) child).getChildCount(); j++) {
                                final View innerChild = ((LinearLayout) child).getChildAt(j);
                                if (innerChild.getClass() == TextInputLayout.class) {
                                    final AutoCompleteTextView _attv = (AutoCompleteTextView) ((FrameLayout) ((TextInputLayout) innerChild).getChildAt(0)).getChildAt(0);
                                    _attv.setAdapter(prAdapter);
                                    _attv.setThreshold(1);
                                    _attv.setText("");
                                    _attv.setTag("");
                                    _attv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                                            Pdp pro = (Pdp) arg0.getAdapter().getItem(arg2);
                                            _attv.setText(pro.getName() + ", " + pro.getPotential());
                                            _attv.setTag(pro);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Pdp>> call, Throwable t) {

            }
        });
    }

    private LinearLayout getNewLinearL(int id) {
        LinearLayout l = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 10);
        l.setLayoutParams(params);
        l.setId(id);
        return l;
    }

    private TextInputLayout getNewTInputL() {
        TextInputLayout t = new TextInputLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 10);
        t.setLayoutParams(params);
        return t;
    }

    private TextInputLayout getNewTInputL(float weight) {
        TextInputLayout t = new TextInputLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, weight);
        params.setMargins(0, 0, 0, 10);
        t.setLayoutParams(params);
        return t;
    }

    private AutoCompleteTextView getNewACTV(int id) {
        final AutoCompleteTextView actv = new AutoCompleteTextView(this);
        actv.setId(id);
        actv.setHint(getResources().getString(R.string.hint_product));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 10);
        actv.setLayoutParams(params);
        actv.setHintTextColor(ContextCompat.getColor(getApplicationContext(), R.color.input_login_hint));

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Products prClient = retrofit.create(Products.class);
        Call<List<Pdp>> call = prClient.getPdps(authHeader, String.valueOf(doctorId));

        call.enqueue(new Callback<List<Pdp>>() {
            @Override
            public void onResponse(Call<List<Pdp>> call, Response<List<Pdp>> response) {
                if (response.isSuccessful()) {
                    List<Pdp> pdps = response.body();
                    prAdapter = new AutoCompleteProductAdapater(getApplicationContext(), pdps);

                    actv.setAdapter(prAdapter);
                    actv.setThreshold(1);
                }
            }

            @Override
            public void onFailure(Call<List<Pdp>> call, Throwable t) {

            }
        });

        actv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Pdp pro = (Pdp) arg0.getAdapter().getItem(arg2);
                actv.setText(pro.getName() + ", " + pro.getPotential());
                actv.setTag(pro);
            }
        });
        return actv;
    }

    private EditText getEDT(int id){
        EditText e = new EditText(this);
        e.setId(id);
        e.setHint(getResources().getString(R.string.hint_samplequantity));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 10, 0, 10);
        e.setLayoutParams(params);
//        e.setInputType(InputType.TYPE_CLASS_NUMBER);
        e.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.input_login));
        e.setHintTextColor(ContextCompat.getColor(getApplicationContext(), R.color.input_login_hint));
        return e;
    }

    private ImageView getAddBtn(int id) {
        final ImageView i = new ImageView(this);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.07f);
        params.gravity = Gravity.CENTER_VERTICAL;
        i.setLayoutParams(params);
        i.setId(id);

        Drawable drw = getResources().getDrawable(R.drawable.ic_add_circle_outline_black_24dp);
        final Drawable newDrawable = DrawableCompat.wrap(drw);
        DrawableCompat.setTint(newDrawable, ContextCompat.getColor(getApplicationContext(), R.color.red));
        i.setBackground(drw);
        i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.red));
        i.setTag(0);
        i.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
            if ((int) v.getTag() == 0) {
                LinearLayout _ll = (LinearLayout) v.getParent();
                int index = container.indexOfChild(_ll);
                index+=1;

                AutoCompleteTextView actv = getNewACTV(ctrlId+=1);
                TextInputLayout tl = getNewTInputL(0.92f);
                TextInputLayout tlQty = getNewTInputL();
                LinearLayout ll = getNewLinearL(ctrlId+=1);
                EditText edt = getEDT(ctrlId+=1);
                ImageView iv = getAddBtn(ctrlId+=1);

                tl.addView(actv);
                tlQty.addView(edt);
                ll.addView(tl);
                ll.addView(iv);

                container.addView(ll, index+=1);
                container.addView(tlQty, index+=1);

                Drawable drw = getResources().getDrawable(R.drawable.ic_remove_circle_outline_black_24dp);
                final Drawable newDrawable = DrawableCompat.wrap(drw);
                DrawableCompat.setTint(newDrawable, ContextCompat.getColor(getApplicationContext(), R.color.red));
                v.setBackground(drw);
                v.setTag(1);
            } else {
                int _imgvId = v.getId();
                int _edtvId = v.getId() - 1;
                int _llId = v.getId() - 2;

                EditText edt = (EditText) container.findViewById(_edtvId);
                TextInputLayout tlQty = (TextInputLayout) edt.getParent().getParent();
                LinearLayout ll = (LinearLayout) container.findViewById(_llId);
                ImageView iv = (ImageView) container.findViewById(_imgvId);

                container.removeView(tlQty);
                container.removeView(iv);
                container.removeView(ll);
            }
            }
        });

        return i;
    }


    class SpinnerItem {
        public SpinnerItem(String label, String key) {
            this.label = label;
            this.key = key;
        }

        String label;
        String key;
    }

    class CustomAdapter extends ArrayAdapter<SpinnerItem> {

        public CustomAdapter(Context context, int item_layout, List<SpinnerItem> objects) {
            super(context, item_layout, objects);
        }

        @Override //don't override if you don't want the default spinner to be a two line view
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView);
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            return initView(position, convertView);
        }

        private View initView(int position, View convertView) {
            if(convertView == null)
                convertView = View.inflate(getContext(),
                        R.layout.spinner_row,
                        null);
            TextView tvText1 = (TextView)convertView.findViewById(R.id.text1);
            tvText1.setText(getItem(position).label);
            tvText1.setTag(getItem(position).key);
            return convertView;
        }
    }
}
