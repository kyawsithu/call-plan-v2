package com.ktz.callplan.entities;

/**
 * Created by kyawsithu on 12/24/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Schedule {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pdp_id")
    @Expose
    private Integer pdpId;
    @SerializedName("mr_id")
    @Expose
    private Integer mrId;
    @SerializedName("other_investment")
    @Expose
    private Integer otherInvestment;
    @SerializedName("sample_qty")
    @Expose
    private Integer sampleQty;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("schedule_date")
    @Expose
    private String scheduleDate;
    @SerializedName("call_priority_id")
    @Expose
    private Integer callPriorityId;
    @SerializedName("latlong")
    @Expose
    private Object latlong;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPdpId() {
        return pdpId;
    }

    public void setPdpId(Integer pdpId) {
        this.pdpId = pdpId;
    }

    public Integer getMrId() {
        return mrId;
    }

    public void setMrId(Integer mrId) {
        this.mrId = mrId;
    }

    public Integer getOtherInvestment() {
        return otherInvestment;
    }

    public void setOtherInvestment(Integer otherInvestment) {
        this.otherInvestment = otherInvestment;
    }

    public Integer getSampleQty() {
        return sampleQty;
    }

    public void setSampleQty(Integer sampleQty) {
        this.sampleQty = sampleQty;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public Integer getCallPriorityId() {
        return callPriorityId;
    }

    public void setCallPriorityId(Integer callPriorityId) {
        this.callPriorityId = callPriorityId;
    }

    public Object getLatlong() {
        return latlong;
    }

    public void setLatlong(Object latlong) {
        this.latlong = latlong;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}