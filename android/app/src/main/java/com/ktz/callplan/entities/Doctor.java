package com.ktz.callplan.entities;

import android.os.Parcel;
import android.os.Parcelable;

//import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yoon on 1/18/2017.
 */

public class Doctor implements Parcelable {

    private int id;
    private String name;
    private String designation;
    private String region;
    private String speciality;
    private String potential;
    private String profile;
    private int designation_id;
    private int potential_id;
    private int region_id;
    private int profile_id;
    private int speciality_id;
    private String created_at;
    private String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getPotential() {
        return potential;
    }

    public void setPotential(String potential) {
        this.potential = potential;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public int getDesignation_id() {
        return designation_id;
    }

    public void setDesignation_id(int designation_id) {
        this.designation_id = designation_id;
    }

    public int getPotential_id() {
        return potential_id;
    }

    public void setPotential_id(int potential_id) {
        this.potential_id = potential_id;
    }

    public int getRegion_id() {
        return region_id;
    }

    public void setRegion_id(int region_id) {
        this.region_id = region_id;
    }

    public int getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(int profile_id) {
        this.profile_id = profile_id;
    }

    public int getSpeciality_id() {
        return speciality_id;
    }

    public void setSpeciality_id(int speciality_id) {
        this.speciality_id = speciality_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


//    public Doctor(String key, String name, String designation, String region, String speciality, String potential, String profile) {
//        this.key = key;
//        this.name = name;
//        this.designation = designation;
//        this.region = region;
//        this.speciality = speciality;
//        this.potential = potential;
//        this.profile = profile;
//    }
//
//    public Doctor() {
//        // Default constructor required for calls to DataSnapshot.getValue(CallDetails.class)
//    }
//
//    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("name", name);
        result.put("designation", designation);
        result.put("region", region);
        result.put("potential", potential);
        result.put("speciality", speciality);
        result.put("profile", profile);
        result.put("designation_id", designation_id);
        result.put("region_id", region_id);
        result.put("potential_id", potential_id);
        result.put("speciality_id", speciality_id);
        result.put("profile_id", profile_id);
        return result;
    }

    // Parcelling part
    public Doctor(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.designation = in.readString();
        this.region = in.readString();
        this.speciality = in.readString();
        this.potential = in.readString();
        this.profile = in.readString();
        this.designation_id = in.readInt();
        this.region_id = in.readInt();
        this.speciality_id = in.readInt();
        this.potential_id = in.readInt();
        this.profile_id = in.readInt();
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.designation);
        dest.writeString(this.region);
        dest.writeString(this.speciality);
        dest.writeString(this.potential);
        dest.writeString(this.profile);
        dest.writeInt(this.designation_id);
        dest.writeInt(this.region_id);
        dest.writeInt(this.speciality_id);
        dest.writeInt(this.potential_id);
        dest.writeInt(this.profile_id);

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Doctor createFromParcel(Parcel in) {
            return new Doctor(in);
        }

        public Doctor[] newArray(int size) {
            return new Doctor[size];
        }
    };
}
