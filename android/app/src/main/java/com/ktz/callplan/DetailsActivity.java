package com.ktz.callplan;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ktz.callplan.api.Schedules;
import com.ktz.callplan.entities.Doctor;
import com.ktz.callplan.entities.FullSchedule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yoon on 1/18/2017.
 */

public class DetailsActivity extends AppCompatActivity {
    private static final String TAG = "DetailsActivity";

    @BindView(R.id.callpriority)
    TextView callpriority;

    @BindView(R.id.calldate)
    TextView calldate;

    @BindView(R.id.doctor)
    TextView doctor;

    @BindView(R.id.product)
    TextView product;

    @BindView(R.id.pdp)
    TextView pdp;

    @BindView(R.id.perception)
    TextView perception;

    @BindView(R.id.samplequantity)
    TextView samplequantity;

    @BindView(R.id.otherinvestment)
    TextView otherinvestment;

    @BindView(R.id.btn_markasfinish)
    Button btnFinish;

    @BindView(R.id.btn_cancel)
    Button btnDelete;

    @BindView(R.id.btn_edit)
    Button btnEdit;

    @BindView(R.id.btnParent)
    RelativeLayout btnParent;

    private SimpleDateFormat dateFormatter;

    private FullSchedule schedule;
    private String latlong;
    private Doctor doc;
    private KTZApp myApp = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        actionBarSetup();

        myApp = ((KTZApp)getApplicationContext());

        Bundle data = getIntent().getExtras();
        schedule = (FullSchedule) data.getParcelable("FullSchedule");
        latlong = data.getString("latlong");

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        String inputString = schedule.getScheduleDate() + " 00:00:00.000";
        Date date = null;
        try {
            date = dateFormatter.parse(inputString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        callpriority.setText(schedule.getCallPriority());
        calldate.setText(dateFormatter.format(date).toString());
        doctor.setText(schedule.getDoctor());
        product.setText(schedule.getProduct());
        pdp.setText(schedule.getPotential());
        perception.setText(schedule.getPerception());
        samplequantity.setText(String.valueOf(schedule.getSampleQty()));
        otherinvestment.setText(String.valueOf(schedule.getOtherInvestment()));

        if (schedule.getStatus() != 0) {
            btnFinish.setVisibility(View.INVISIBLE);
            btnDelete.setVisibility(View.INVISIBLE);
            btnParent.setVisibility(View.GONE);
        } else {
            btnEdit.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
    }

    /**
     * Sets the Action Bar for new Android versions.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("");
            ab.setSubtitle("Call Details");
            ab.setDisplayHomeAsUpEnabled(true);
            //ab.setHomeButtonEnabled(true);
        }
    }

    @OnClick(R.id.btn_edit)
    public void onEditClick() {
        Intent intent = new Intent(getApplicationContext(), EditPdpActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.finish();
        intent.putExtra("Schedule", schedule);
        startActivity(intent);
    }

    @OnClick(R.id.btn_markasfinish)
    public void onClick() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Schedules shclient = retrofit.create(Schedules.class);
        Call<Boolean> call = shclient.scheduleComplete(authHeader, schedule.getId(), latlong);

        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(DetailsActivity.this, "Your call has been marked as FINISHED.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("Failed", "Failed");
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                t.printStackTrace();
                Log.e("Failed", "Failed");
            }
        });
    }

    @OnClick(R.id.btn_cancel)
    public void onDeleteClick() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Schedules shclient = retrofit.create(Schedules.class);
        Call<Boolean> call = shclient.scheduleDelete(authHeader, schedule.getId());

        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(DetailsActivity.this, "Your call has been deleted.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("Delete Failed", "Failed");
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                t.printStackTrace();
                Log.e("Delete Failed", "Failed");
            }
        });

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
