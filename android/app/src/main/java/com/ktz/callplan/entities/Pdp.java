package com.ktz.callplan.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kyawsithu on 12/10/17.
 */

public class Pdp implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("perception")
    @Expose
    private String perception;
    @SerializedName("pdp_id")
    @Expose
    private Integer pdpId;
    @SerializedName("potential")
    @Expose
    private String potential;
    public final static Parcelable.Creator<Pdp> CREATOR = new Creator<Pdp>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Pdp createFromParcel(Parcel in) {
            return new Pdp(in);
        }

        public Pdp[] newArray(int size) {
            return (new Pdp[size]);
        }

    }
            ;

    protected Pdp(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.perception = ((String) in.readValue((String.class.getClassLoader())));
        this.pdpId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.potential = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Pdp() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPerception() {
        return perception;
    }

    public void setPerception(String perception) {
        this.perception = perception;
    }

    public Integer getPdpId() {
        return pdpId;
    }

    public void setPdpId(Integer pdpId) {
        this.pdpId = pdpId;
    }

    public String getPotential() {
        return potential;
    }

    public void setPotential(String potential) {
        this.potential = potential;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(perception);
        dest.writeValue(pdpId);
        dest.writeValue(potential);
    }

    public int describeContents() {
        return 0;
    }

}