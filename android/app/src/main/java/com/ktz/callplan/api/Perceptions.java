package com.ktz.callplan.api;

import com.ktz.callplan.entities.Perception;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by kyawsithu on 12/26/17.
 */

public interface Perceptions {
    @GET("/api/perceptions")
    Call<List<Perception>> getPerceptions(@Header("Authorization") String authHeader);
}
