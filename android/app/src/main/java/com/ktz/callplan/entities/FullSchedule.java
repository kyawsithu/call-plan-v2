package com.ktz.callplan.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kyawsithu on 12/24/17.
 */
public class FullSchedule implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pdp_id")
    @Expose
    private Integer pdpId;
    @SerializedName("mr_id")
    @Expose
    private Integer mrId;
    @SerializedName("other_investment")
    @Expose
    private Integer otherInvestment;
    @SerializedName("sample_qty")
    @Expose
    private Integer sampleQty;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("schedule_date")
    @Expose
    private String scheduleDate;
    @SerializedName("call_priority_id")
    @Expose
    private Integer callPriorityId;
    @SerializedName("latlong")
    @Expose
    private String latlong;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("call_priority")
    @Expose
    private String callPriority;
    @SerializedName("perception")
    @Expose
    private String perception;
    @SerializedName("potential")
    @Expose
    private String potential;
    @SerializedName("doctor")
    @Expose
    private String doctor;
    @SerializedName("product")
    @Expose
    private String product;
    public final static Parcelable.Creator<FullSchedule> CREATOR = new Creator<FullSchedule>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FullSchedule createFromParcel(Parcel in) {
            return new FullSchedule(in);
        }

        public FullSchedule[] newArray(int size) {
            return (new FullSchedule[size]);
        }

    }
            ;

    protected FullSchedule(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.pdpId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.mrId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.otherInvestment = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.sampleQty = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.scheduleDate = ((String) in.readValue((String.class.getClassLoader())));
        this.callPriorityId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.latlong = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.callPriority = ((String) in.readValue((String.class.getClassLoader())));
        this.perception = ((String) in.readValue((String.class.getClassLoader())));
        this.potential = ((String) in.readValue((String.class.getClassLoader())));
        this.doctor = ((String) in.readValue((String.class.getClassLoader())));
        this.product = ((String) in.readValue((String.class.getClassLoader())));
    }

    public FullSchedule() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPdpId() {
        return pdpId;
    }

    public void setPdpId(Integer pdpId) {
        this.pdpId = pdpId;
    }

    public Integer getMrId() {
        return mrId;
    }

    public void setMrId(Integer mrId) {
        this.mrId = mrId;
    }

    public Integer getOtherInvestment() {
        return otherInvestment;
    }

    public void setOtherInvestment(Integer otherInvestment) {
        this.otherInvestment = otherInvestment;
    }

    public Integer getSampleQty() {
        return sampleQty;
    }

    public void setSampleQty(Integer sampleQty) {
        this.sampleQty = sampleQty;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public Integer getCallPriorityId() {
        return callPriorityId;
    }

    public void setCallPriorityId(Integer callPriorityId) {
        this.callPriorityId = callPriorityId;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCallPriority() {
        return callPriority;
    }

    public void setCallPriority(String callPriority) {
        this.callPriority = callPriority;
    }

    public String getPerception() {
        return perception;
    }

    public void setPerception(String perception) {
        this.perception = perception;
    }

    public String getPotential() {
        return potential;
    }

    public void setPotential(String potential) {
        this.potential = potential;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(pdpId);
        dest.writeValue(mrId);
        dest.writeValue(otherInvestment);
        dest.writeValue(sampleQty);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(scheduleDate);
        dest.writeValue(callPriorityId);
        dest.writeValue(latlong);
        dest.writeValue(status);
        dest.writeValue(callPriority);
        dest.writeValue(perception);
        dest.writeValue(potential);
        dest.writeValue(doctor);
        dest.writeValue(product);
    }

    public int describeContents() {
        return 0;
    }

}