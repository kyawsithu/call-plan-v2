package com.ktz.callplan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ktz.callplan.entities.Perception;

import java.util.List;

/**
 * Created by kyawsithu on 12/27/17.
 */

public class PerceptionAdapter extends BaseAdapter {
    Context context;
    List<Perception> perceptions;
    LayoutInflater inflater;

    public PerceptionAdapter(Context applicationContext, List<Perception> perceptions) {
        this.context = applicationContext;
        this.perceptions = perceptions;
        inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return perceptions.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.spinner_row, null);
        TextView txt1 = (TextView) view.findViewById(R.id.text1);
        txt1.setText(perceptions.get(i).getName());
        return view;
    }
}