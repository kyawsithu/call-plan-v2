package com.ktz.callplan.api;

import com.ktz.callplan.entities.FullSchedule;
import com.ktz.callplan.entities.Schedule;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by kyawsithu on 12/24/17.
 */

public interface Schedules {
    @GET("/api/schedulesByMr")
    Call<List<FullSchedule>> getSchedulesByMr(@Header("Authorization") String authHeader, @Query("mrid") String mrid);

    @POST("/api/scheduleAdd")
    Call<Integer> createSchedule(@Header("Authorization") String authHeader, @Body Schedule schedule);

    @FormUrlEncoded
    @POST("/api/scheduleComplete")
    Call<Boolean> scheduleComplete(@Header("Authorization") String authHeader, @Field("id") int id, @Field("latlong") String latlong);

    @FormUrlEncoded
    @POST("/api/scheduleDelete")
    Call<Boolean> scheduleDelete(@Header("Authorization") String authHeader, @Field("id") int id);
}
