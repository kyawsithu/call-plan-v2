package com.ktz.callplan;

/**
 * Created by kyawsithu on 12/10/17.
 */

public interface Constant {
//    public static final String API_URL = "http://192.168.1.116:8000";
    public static final String API_URL = "http://174.138.16.83";
    public static final String PREF = "SharedPrefs";
    public static final String LOGIN_PREF = "IsLoggedIn";
    public static final String USER_PREF = "UserInfo";
    public static final String AUTH_HEADER = "AuthHeader";
}
