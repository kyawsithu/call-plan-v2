package com.ktz.callplan.api;

import com.ktz.callplan.entities.CallPriority;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by kyawsithu on 12/10/17.
 */

public interface CallPriorities {
    @GET("/api/callpriorities")
    Call<List<CallPriority>> getCallPriority(@Header("Authorization") String authHeader);
}


