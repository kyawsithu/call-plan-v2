package com.ktz.callplan;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ktz.callplan.api.Doctors;
import com.ktz.callplan.api.Schedules;
import com.ktz.callplan.api.Users;
import com.ktz.callplan.decorators.EventDecorator;
import com.ktz.callplan.decorators.HighlightWeekendsDecorator;
import com.ktz.callplan.decorators.OneDayDecorator;
import com.ktz.callplan.entities.Doctor;
import com.ktz.callplan.entities.FullSchedule;
import com.ktz.callplan.entities.User;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity implements OnDateSelectedListener, LocationListener {

    private static final String TAG = "MainActivity";
    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    private SimpleDateFormat dateFormatter;
    private ProgressDialog progressDialog;
    private User user;

    List<CalendarDay> dates;
    List<FullSchedule> schedules;
    List<Doctor> doctors;
    List<FullSchedule> onedaySchedules = new ArrayList<>();

    private RecyclerView mRecyclerView;
    private ListAdapter mAdapter;

    private CalendarDay mDate = null;
    private KTZApp myApp = null;

    @BindView(R.id.calendarView) MaterialCalendarView widget;

    // Location related
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    public Double latitude;
    public Double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Progress Dialog
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Fetching data..."); // Setting Message
        progressDialog.setTitle("Loading"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        // progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        // Get App variable
        myApp = ((KTZApp)getApplicationContext());

        // Declare Gson and get user shared preference
        Gson gson = new Gson();
        final SharedPreferences settings = getSharedPreferences(Constant.PREF, MODE_PRIVATE);
        String json = settings.getString(Constant.USER_PREF, "");
        user = gson.fromJson(json, User.class);

        String authHeader = settings.getString(Constant.AUTH_HEADER, "");
        myApp.setHashKey(authHeader);

        actionBarSetup();
        getDoctors();

        userExistCheck();

        // widget.setTileWidthDp(45);
        widget.setTileHeightDp(32);
        widget.setOnDateChangedListener(this);
        widget.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        widget.setAllowClickDaysOutsideCurrentMonth(true);
        widget.canGoBack();
        widget.canGoForward();

        Calendar instance = Calendar.getInstance();
        widget.setSelectedDate(instance.getTime());

        Calendar instance1 = Calendar.getInstance();
        instance1.set(instance1.get(Calendar.YEAR), Calendar.JANUARY, 1);
        instance1.add(Calendar.YEAR, -1);

        Calendar instance2 = Calendar.getInstance();
        instance2.set(instance2.get(Calendar.YEAR), Calendar.DECEMBER, 31);
        instance2.add(Calendar.YEAR, 1);

        widget.state().edit()
                .setMinimumDate(instance1.getTime())
                .setMaximumDate(instance2.getTime())
                .commit();

        widget.addDecorator(new HighlightWeekendsDecorator());

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new ListAdapter(this, onedaySchedules);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        // mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);

        setEventDates(user);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        // Location related
        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            showSettingsAlert();
            getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            // check permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    Log.d(TAG, "Permission requests");
                    canGetLocation = false;
                }
            }

            // get location
            getLocation();
        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        setEventDates(user);
    }

    /**
     * Sets the Action Bar for new Android versions.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("");
            ab.setSubtitle("Schedules");
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_call:
                Intent call = new Intent(MainActivity.this, CreateCallActivity.class);
                startActivity(call);
                return true;
            case R.id.menu_doctor_report:
                Intent report1 = new Intent(MainActivity.this, ReportActivity.class);
                report1.putExtra("User", user);
                report1.putExtra("Path", "/admin/mreports/doctor?mr_group=" + user.getMrGroup());
                report1.putExtra("title", getResources().getString(R.string.menu_doctor_report));
                startActivity(report1);
                return true;
            case R.id.menu_doctorstatus_report:
                Intent report2 = new Intent(MainActivity.this, ReportActivity.class);
                report2.putExtra("User", user);
                report2.putExtra("Path", "/admin/mreports/doctorstatus?mr_group=" + user.getMrGroup());
                report2.putExtra("title", getResources().getString(R.string.menu_doctorstatus_report));
                startActivity(report2);
                return true;
            case R.id.menu_perception_report:
                Intent report3 = new Intent(MainActivity.this, ReportActivity.class);
                report3.putExtra("User", user);
                report3.putExtra("Path", "/admin/mreports/productperception?mr_group=" + user.getMrGroup());
                report3.putExtra("title", getResources().getString(R.string.menu_perception_report));
                startActivity(report3);
                return true;
            case R.id.menu_prescription_report:
                Intent report4 = new Intent(MainActivity.this, ReportActivity.class);
                report4.putExtra("User", user);
                report4.putExtra("Path", "/admin/mreports/productprescription?mr_group=" + user.getMrGroup());
                report4.putExtra("title", getResources().getString(R.string.menu_prescription_report));
                startActivity(report4);
                return true;
            case R.id.menu_call_report:
                Intent report5 = new Intent(MainActivity.this, ReportActivity.class);
                report5.putExtra("User", user);
                report5.putExtra("Path", "/admin/mreports/schedule?mr_group=" + user.getMrGroup() + "&mr=" + user.getId());
                report5.putExtra("title", getResources().getString(R.string.menu_call_report));
                startActivity(report5);
                return true;
            case R.id.menu_logout:
                final SharedPreferences settings = getSharedPreferences(Constant.PREF, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent login = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(login);
                return true;
            case R.id.menu_item_refresh:
                setEventDates(user);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        //If you change a decorate, you need to invalidate decorators
        oneDayDecorator.setDate(date.getDate());
        widget.invalidateDecorators();
        populateRecyclerView(date);
        mDate = date;
    }

//    private void populateRecyclerView(long date){
//        onedaySchedules.clear();
//        for(FullSchedule call : schedules){
//            if (dateFormatter.format(call.getScheduleDate()).equals(dateFormatter.format(date))){
//                onedaySchedules.add(call);
//            }
//        }
//        mAdapter.notifyDataSetChanged();
//    }

    public void populateRecyclerView(CalendarDay date){
        onedaySchedules.clear();
        for(FullSchedule call : schedules){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); //2017-12-20
            String inputString = call.getScheduleDate() + " 00:00:00.000";
            Date scheduleDate = null;
            try {
                scheduleDate = sdf.parse(inputString);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (dateFormatter.format(scheduleDate).equals(dateFormatter.format(date.getDate()))){
                onedaySchedules.add(call);
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    public void setEventDates(User user) {
        progressDialog.show();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Schedules schClient = retrofit.create(Schedules.class);
        Call<List<FullSchedule>> call = schClient.getSchedulesByMr(authHeader, String.valueOf(user.getId()));

        call.enqueue(new Callback<List<FullSchedule>>() {
            @Override
            public void onResponse(Call<List<FullSchedule>> call, Response<List<FullSchedule>> response) {
                if (response.isSuccessful()) {
                    schedules = response.body();
                    dates = new ArrayList<>();
                    Calendar cal = Calendar.getInstance();
                    for(FullSchedule s : schedules){
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); //2017-12-20
                        String inputString = s.getScheduleDate() + " 00:00:00.000";
                        Date date = null;
                        try {
                            date = sdf.parse(inputString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        cal.setTimeInMillis(date.getTime());
                        CalendarDay day = CalendarDay.from(cal);
                        dates.add(day);
                    }
                    widget.addDecorator(new EventDecorator(Color.RED, dates));
                    progressDialog.dismiss();
                    populateRecyclerView(widget.getSelectedDate());
                }
                else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<FullSchedule>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getDoctors() {
        doctors = new ArrayList<Doctor>();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Doctors drClient = retrofit.create(Doctors.class);
        Call<List<Doctor>> call = drClient.getDoctors(authHeader);

        call.enqueue(new Callback<List<Doctor>>() {
            @Override
            public void onResponse(Call<List<Doctor>> call, Response<List<Doctor>> response) {
                if (response.isSuccessful()) {
                    doctors = response.body();
                }
            }

            @Override
            public void onFailure(Call<List<Doctor>> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.fab_add)
    public void onClick() {
        Intent call = new Intent(MainActivity.this, CreateCallActivity.class);
        startActivity(call);
    }

    public void APICall(final String message, final FullSchedule schedule, final int operation)
    {
        int id = schedule.getId();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Schedules shclient = retrofit.create(Schedules.class);
        Call<Boolean> call;
        if (operation == 1)
        {
            call = shclient.scheduleComplete(authHeader, id, latitude.toString() + "," + longitude.toString());
        } else {
            call = shclient.scheduleDelete(authHeader, id);
        }

        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    if (operation == 1){

                    }
                    setEventDates(user);
                } else {
                    Log.e(message + " - Failed", "Failed");
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                t.printStackTrace();
                Log.e(message + " - Failed", "Failed");
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged");
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {}

    @Override
    public void onProviderEnabled(String s) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String s) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else {
                    loc.setLatitude(0);
                    loc.setLongitude(0);
                    updateUI(loc);
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void updateUI(Location loc) {
        Log.d(TAG, "updateUI");
        latitude = loc.getLatitude();
        longitude = loc.getLongitude();

    }

    private void userExistCheck(){
        Gson gson = new Gson();
        final SharedPreferences settings = getSharedPreferences(Constant.PREF, MODE_PRIVATE);
        String json = settings.getString(Constant.USER_PREF, "");
        User user = gson.fromJson(json, User.class);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        String authHeader = myApp.getHashKey();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Users userClient = retrofit.create(Users.class);
        Call<User> call = userClient.getUser(authHeader, user.getEmail());

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {

                }else{
                    SharedPreferences.Editor editor = settings.edit();
                    editor.clear();
                    editor.commit();

                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                Log.e("Failed", "Failed");
            }
        });
    }
}
