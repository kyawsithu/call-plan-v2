package com.ktz.callplan;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ktz.callplan.entities.FullSchedule;
import com.ktz.callplan.entities.User;

import java.util.List;
/**
 * Created by Yoon on 1/17/2017.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    Context mContext;

    private List<FullSchedule> callList;
    private KTZApp myApp = null;
    private User user;
    public ListAdapter(Context context) {
        this.mContext = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout rowview;
        public TextView doctor, product, region, status, btnFinish, btnDelete;

        public ViewHolder(View view) {
            super(view);
            doctor = (TextView) view.findViewById(R.id.doctor);
            product = (TextView) view.findViewById(R.id.product);
            region = (TextView) view.findViewById(R.id.region);
            status = (TextView) view.findViewById(R.id.status);

            btnFinish = (TextView) view.findViewById(R.id.txtFinish);
            btnDelete = (TextView) view.findViewById(R.id.txtDelete);

            rowview = (RelativeLayout) view.findViewById(R.id.rowview);
        }
    }

    public ListAdapter(Context context, List<FullSchedule> callList) {
        this.callList = callList;
        this.mContext = context;
        myApp = ((KTZApp) context.getApplicationContext());
    }

    @Override
    public int getItemCount() {
        return callList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FullSchedule call = callList.get(position);

        Gson gson = new Gson();
        final SharedPreferences settings = mContext.getSharedPreferences(Constant.PREF, 0);
        String json = settings.getString(Constant.USER_PREF, "");
        user = gson.fromJson(json, User.class);

        String[] status = {
                "In-Progress",
                "Finished",
                "Cancelled"
        };

        switch(call.getStatus()) {
            case 0:
                holder.status.setTextColor(mContext.getResources().getColor(R.color.blue));
                holder.btnFinish.setTextColor(mContext.getResources().getColor(R.color.green));
                holder.btnDelete.setTextColor(mContext.getResources().getColor(R.color.red));
                holder.btnDelete.setEnabled(true);
                holder.btnFinish.setEnabled(true);
                break;
            case 1:
                holder.status.setTextColor(mContext.getResources().getColor(R.color.green));
                holder.btnFinish.setTextColor(mContext.getResources().getColor(R.color.light_gray));
                holder.btnDelete.setTextColor(mContext.getResources().getColor(R.color.light_gray));
                holder.btnDelete.setEnabled(false);
                holder.btnFinish.setEnabled(false);
                break;
            case 2:
                holder.status.setTextColor(mContext.getResources().getColor(R.color.red));
                break;
            default:
                break;
        }

        holder.doctor.setText(call.getDoctor());
        holder.product.setText(call.getProduct());
        holder.region.setText(call.getPotential());
        holder.status.setText(status[call.getStatus()].toString());

        holder.rowview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do on click stuff
                //Toast.makeText(mContext, call.getKey().toString(), Toast.LENGTH_SHORT).show();
                String latlong = ((MainActivity) mContext).latitude + "," + ((MainActivity) mContext).longitude;
                Intent details = new Intent(mContext, DetailsActivity.class);
                details.putExtra("FullSchedule", call);
                details.putExtra("latlong", latlong);
                mContext.startActivity(details);
            }
        });

        holder.btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do on click stuff
                ((MainActivity) mContext).APICall("Your schedule has been marked as finished.", call, 1);
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //Do on click stuff
                ((MainActivity) mContext).APICall("Your schedule has been successfully deleted.", call, 2);
            }
        });
    }
}