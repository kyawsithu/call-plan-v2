package com.ktz.callplan.entities;

//import com.google.firebase.database.Exclude;
//import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

//import com.google.firebase.database.Exclude;
//import com.google.firebase.database.IgnoreExtraProperties;
//import com.google.firebase.database.ServerValue;
//
//import static com.ktz.callplan.R.id.callpriority;
//import static com.ktz.callplan.R.id.pdp;

/**
 * Created by Yoon on 1/16/2017.
 */
//@IgnoreExtraProperties
public class ProductDoctor {
    public String doctor;
    public String doctorId;
    public String product;
    public String productId;
    public String pdp;
    public String perception;

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPdp() {
        return pdp;
    }

    public void setPdp(String pdp) {
        this.pdp = pdp;
    }

    public String getPerception() {
        return perception;
    }

    public void setPerception(String perception) {
        this.perception = perception;
    }

    public ProductDoctor() {
        // Default constructor required for calls to DataSnapshot.getValue(CallDetails.class)
    }

    public ProductDoctor(String doctor, String doctorId, String product, String productId, String pdp, String perception) {
        this.doctor = doctor;
        this.doctorId = doctorId;
        this.product = product;
        this.productId = productId;
        this.pdp = pdp;
        this.perception = perception;
    }

//    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("doctor", doctor);
        result.put("doctorId", doctorId);
        result.put("product", product);
        result.put("productId", productId);
        result.put("pdp", pdp);
        result.put("perception", perception);
        return result;
    }
}
