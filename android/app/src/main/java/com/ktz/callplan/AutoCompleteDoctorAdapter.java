package com.ktz.callplan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.ktz.callplan.entities.Doctor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yoon on 2/8/2017.
 */

public class AutoCompleteDoctorAdapter extends ArrayAdapter<Doctor> {
    private Context context;
    private final List<Doctor> docs;
    private List<Doctor> filteredDocs = new ArrayList<>();

    public AutoCompleteDoctorAdapter(Context context, List<Doctor> docs) {
        super(context, 0, docs);
        this.context = context;
        this.docs = docs;
    }

    @Override
    public int getCount() {
        return filteredDocs.size();
    }

    @Override
    public Filter getFilter() {
        return new DoctorFilter(this, docs);
    }

    @Override
    public Doctor getItem(int position) {
        return filteredDocs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item from filtered list.
        Doctor doc = filteredDocs.get(position);

        // Inflate your custom row layout as usual.
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.spinner_row, parent, false);
        }

        if (doc != null){
            TextView tvName = (TextView) convertView.findViewById(R.id.text1);
            tvName.setText(doc.getName() + ", " + doc.getDesignation() + ", " + doc.getRegion());
        }
        return convertView;
    }

    class DoctorFilter extends Filter {

        AutoCompleteDoctorAdapter adapter;
        List<Doctor> originalList;
        List<Doctor> filteredList;

        public DoctorFilter(AutoCompleteDoctorAdapter adapter, List<Doctor> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = originalList;
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                // Your filtering logic goes in here
                for (final Doctor doc : originalList) {
                    if (doc.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(doc);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredDocs.clear();
            adapter.filteredDocs.addAll((List) results.values);
            adapter.notifyDataSetChanged();
        }
    }
}

