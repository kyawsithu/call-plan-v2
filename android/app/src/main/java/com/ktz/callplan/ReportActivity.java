package com.ktz.callplan;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kyawsithu on 12/25/17.
 */

public class ReportActivity extends AppCompatActivity {
    @BindView(R.id.rpWebview)
    WebView rpWebview;

    private String path;
    private String title;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);

        // Progress Dialog
        progressDialog = new ProgressDialog(ReportActivity.this);
        progressDialog.setMessage("Fetching data..."); // Setting Message
        progressDialog.setTitle("Loading"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Bundle data = getIntent().getExtras();
        path = data.getString("Path");
        title = data.getString("title");


        rpWebview.getSettings().setJavaScriptEnabled(true);

        final Activity activity = this;
        rpWebview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                activity.setProgress(progress * 1000);

                activity.setTitle("Loading...");
                activity.setProgress(progress * 100);
                if(progress == 100) {
                    activity.setTitle("Call Plan - " + title);
                    progressDialog.dismiss();
                }
            }
        });
        rpWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                progressDialog.dismiss();
                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        rpWebview.loadUrl(Constant.API_URL + path);
    }
}
