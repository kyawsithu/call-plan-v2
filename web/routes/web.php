<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    // My overwrite
    Route::get('doctorsexport', function(){
        $table = \DB::table('doctors')
            ->join("potentials", "potentials.id", "=", "doctors.potential_id")
            ->join("designations", "designations.id", "=", "doctors.designation_id")
            ->join("specialities", "specialities.id", "=", "doctors.speciality_id")
            ->join("profiles", "profiles.id", "=", "doctors.profile_id")
            ->join("regions", "regions.id", "=", "doctors.region_id")
            ->select(
                'doctors.id', 
                'doctors.name', 
                'potentials.name as potential',
                'designations.name as designation',
                'specialities.name as speciality',
                'profiles.name as profile',
                'regions.name as region'
            )
            ->get();

        $array = json_decode(json_encode($table), true); 

        $filename = storage_path()."/doctors.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array(
            'Id', 
            'Name', 
            'Potential', 
            'Designation', 
            'Speciality', 
            'Profile', 
            'Region'
        ));
    
        foreach($array as $row) {
            fputcsv($handle, array(
                $row['id'], 
                $row['name'],  
                $row['potential'], 
                $row['designation'], 
                $row['speciality'],
                $row['profile'],
                $row['region']
            ));
        }
    
        fclose($handle);
    
        $headers = array(
            'Content-Type' => 'text/csv',
        );

        return Response::download($filename, 'doctors.csv', $headers);
    });
    
    Route::get('reports', 'ReportController@index');
    Route::get('reports/schedule', 'ReportController@schedule');
    Route::get('reports/resource', 'ReportController@resource');
    Route::get('reports/productperception', 'ReportController@productperception');
    Route::get('reports/productprescription', 'ReportController@productprescription');
    Route::get('reports/doctorstatus', 'ReportController@doctorstatus');
    Route::get('reports/doctor', 'ReportController@doctor');
    Route::get('map/index/{latlong}', 'MapController@index');


    // Mobile
    Route::get('mreports/schedule', 'MobileReportController@schedule');
    Route::get('mreports/productperception', 'MobileReportController@productperception');
    Route::get('mreports/productprescription', 'MobileReportController@productprescription');
    Route::get('mreports/doctorstatus', 'MobileReportController@doctorstatus');
    Route::get('mreports/doctor', 'MobileReportController@doctor');
    
});
