<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [], function ($api) {
    $api->group(['middleware' => 'api.auth'], function ($api) {
        // Get
        $api->get('designations', 'App\Http\Controllers\Api\DesignationController@getDesignations');
        $api->get('regions', 'App\Http\Controllers\Api\RegionController@getRegions');
        $api->get('potentials', 'App\Http\Controllers\Api\PotentialController@getPotentials');
        $api->get('prescriptions', 'App\Http\Controllers\Api\PrescriptionController@getPrescriptions');
        $api->get('perceptions', 'App\Http\Controllers\Api\PerceptionController@getPerceptions');
        $api->get('user', 'App\Http\Controllers\Api\UserController@getUser');
        $api->get('callpriorities', 'App\Http\Controllers\Api\CallPriorityController@getCallPriority');
        $api->get('doctors', 'App\Http\Controllers\Api\DoctorController@getDoctors');
        $api->get('products', 'App\Http\Controllers\Api\ProductController@getProducts');
        $api->get('pdps', 'App\Http\Controllers\Api\ProductController@getPDPs');
        $api->get('schedules', 'App\Http\Controllers\Api\ScheduleController@getSchedules');
        $api->get('originalSchedules', 'App\Http\Controllers\Api\ScheduleController@getOriginalSchedules');
        $api->get('schedulesByMr', 'App\Http\Controllers\Api\ScheduleController@getSchedulesByMr');
        
        // Post
        $api->post('scheduleAdd', 'App\Http\Controllers\Api\ScheduleController@scheduleAdd');
        $api->post('scheduleComplete', 'App\Http\Controllers\Api\ScheduleController@scheduleComplete');
        $api->post('scheduleDelete', 'App\Http\Controllers\Api\ScheduleController@scheduleDelete');
        $api->post('pdpEdit', 'App\Http\Controllers\Api\ProductController@editPdp');
    });
    
});