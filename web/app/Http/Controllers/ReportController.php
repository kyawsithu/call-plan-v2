<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use App\CallPriority;
use App\Product;
use App\Speciality;
use App\Region;
use App\Doctor;

class ReportController extends Controller
{
    //
    public function index()
    {
        return view('reports.index');
    }

    public function schedule(Request $request)
    {
        if (!Auth::check()){
            return redirect()->intended('/admin/login');
        } else {
            $param = '';
            $regionparam = '';
            $arr = array(date("Y-m-d"), date("Y-m-d"));
            
            if ($request->input('date') != "")
            {
                $arr = explode(" - ", $request->input('date'));
                $param .= " AND `schedules`.`schedule_date` between '".$arr[0]."' AND '".$arr[1]."'";
            }
            if ($request->input('mr') != "")
            {
                $param .= " AND `schedules`.`mr_id` =".$request->input('mr');
            }
            if ($request->input('callStatus') != "")
            {
                $param .= " AND `schedules`.`status` =".$request->input('callStatus');
            }
            if ($request->input('product') != "")
            {
                $param .= " AND `pdps`.`product_id` =".$request->input('product');
            }
            if ($request->input('region') != "")
            {
                $regionparam .= " AND `users`.`region_id` =".$request->input('region');
            }

            $group_param = '';
            // Check user role and add conditions
            if (Auth::user()->role_id != 1) {
                $group_param = ' AND `group_products`.`group_id` = '.Auth::user()->mr_group.'
                            AND `schedules`.`mr_id` IN (
                                SELECT id FROM users WHERE `mr_group` = '.Auth::user()->mr_group.$regionparam.'
                            )';
            }

            $regions = Region::orderBy('name', 'asc')->get();
            $selectRegions = array();
            foreach($regions as $reg) {
                $selectRegions[$reg->id] = $reg->name;
            }

            $selectCallStatus = array(
                '0' => 'Pre Call',
                '1' => 'Post Call'
            );

            // Check role and filter data
            $mrs = User::orderBy('name', 'asc');
            if (Auth::user()->role_id != 1) {
                $mrs = $mrs->where('mr_group', Auth::user()->mr_group)->get();
            } else {
                $mrs = $mrs->get();
            }
            $selectMrs = array();
            foreach($mrs as $mr) {
                $selectMrs[$mr->id] = $mr->name;
            }

            $products = \DB::table('products')
                ->join('group_products', 'group_products.product_id', '=', 'products.id')
                ->select(
                    'products.id',
                    'products.name')
                ->orderBy('name', 'asc');
            // Check role and filter data
            if (Auth::user()->role_id != 1) {
                $products = $products->select("id", "name")->where('group_id', Auth::user()->mr_group)->get();
            } else {
                $products = $products->select("id", "name")->get();
            }
            
            // $selectProducts = array();
            // foreach($products as $prd) {
            //     $selectProducts[$prd->id] = $prd->name;
            // }
            $selectProducts = $products->toJson();


            $cols = DB::select("call getPotentialLists()");

            $priority = [];
            if ($param != ""){
                $priority = DB::select(
                    'SELECT `products`.`name` as `Product`,'. $cols[0]->cols .' 
                        FROM `potentials` 
                        JOIN `pdps` ON `pdps`.`potential_id` = `potentials`.`id` 
                        JOIN `doctors` ON `doctors`.`id` = `pdps`.`doctor_id` 
                        JOIN `products` ON `products`.`id` = `pdps`.`product_id` 
                        JOIN `group_products` ON `group_products`.`product_id` = `products`.`id`
                        JOIN `schedules` ON `schedules`.`pdp_id` = `pdps`.`id`
                        WHERE 1 '.$group_param.$param.'
                        GROUP BY `products`.`name` ORDER BY `products`.`name`'
                );
            }
            
            return view ('reports.schedule', 
                [
                    'request' => $request,
                    'priority' => $priority, 
                    'callStatus' => $selectCallStatus, 
                    'mr' => $selectMrs,
                    'region' => $selectRegions,
                    'product' => $selectProducts,
                    'date' => $arr
                ]
            );

            // SELECT GROUP_CONCAT(DISTINCT CONCAT( 'ifnull(COUNT(case when potentials.name = ''', potentials.name, ''' then potentials.name end),0) AS `', potentials.name, '`' ) ) INTO @sql FROM potentials;
            
            // SET @sql = CONCAT('SELECT `products`.`name` as `product_name`,', @sql, ' FROM `potentials` 
            //     JOIN `pdps` ON `pdps`.`potential_id` = `potentials`.`id` 
            //     JOIN `doctors` ON `doctors`.`id` = `pdps`.`doctor_id` 
            //     JOIN `products` ON `products`.`id` = `pdps`.`product_id` 
            //     GROUP BY `products`.`name` ORDER BY `products`.`name`');
        
            // PREPARE stmt FROM @sql;
            // EXECUTE stmt;
            // DEALLOCATE PREPARE stmt;"
        }
    }

    public function resource(Request $request)
    {
        if (!Auth::check()){
            return redirect()->intended('/admin/login');
        } else {
            $group_param = '';
            $param = '';
            if (Auth::user()->role_id != 1) {
                $group_param = ' AND `group_products`.`group_id` = '.Auth::user()->mr_group.'
                            AND `schedules`.`mr_id` IN (
                                SELECT id FROM users WHERE `mr_group` = '.Auth::user()->mr_group.'
                        )';
            }

            $arr = array(date("Y-m-d"), date("Y-m-d"));

            if ($request->input('date') != "")
            {
                $arr = explode(" - ", $request->input('date'));
                $param .= " AND `schedules`.`schedule_date` between '".$arr[0]."' AND '".$arr[1]."'";
                // $param .= " AND `schedules`.`schedule_date` ='".$request->input('date')."'";
            }
            if ($request->input('mr') != "")
            {
                $param .= " AND `schedules`.`mr_id` =".$request->input('mr');
            }

            // Check role and filter data
            $mrs = User::orderBy('name', 'asc');
            if (Auth::user()->role_id != 1) {
                $mrs = $mrs->where('mr_group', Auth::user()->mr_group)->get();
            } else {
                $mrs = $mrs->get();
            }
        
            $selectMrs = array();
            foreach($mrs as $mr) {
                $selectMrs[$mr->id] = $mr->name;
            }

            $investment = [];
            if ($param != ""){
                $investment = DB::select(
                    'SELECT `products`.`name` as `product`, 
                            SUM(`schedules`.`other_investment`) as other_investment, 
                            SUM(`schedules`.`sample_qty`) as sample_qty
                        FROM `potentials` 
                        JOIN `pdps` ON `pdps`.`potential_id` = `potentials`.`id` 
                        JOIN `products` ON `products`.`id` = `pdps`.`product_id` 
                        JOIN `group_products` ON `group_products`.`product_id` = `pdps`.`product_id` 
                        JOIN `schedules` ON `schedules`.`pdp_id` = `pdps`.`id`
                        WHERE 1 '.$group_param.$param.'
                        GROUP BY `products`.`name` 
                        ORDER BY `products`.`name`'
                );
            }
            
            return view ('reports.resource', 
                [
                    'investment' => $investment, 
                    'mr' => $selectMrs,   
                    'date' => $arr                 
                ]
            );
        }
    }

    public function productprescription(Request $request)
    {
        if (!Auth::check()){
            return redirect()->intended('/admin/login');
        } else {
            $param = '';
            $group_param = '';
            if (Auth::user()->role_id != 1){
                $group_param = ' AND `group_products`.`group_id` = '.Auth::user()->mr_group;
            }
            if ($request->input('product') != "")
            {
                $param .= " AND `pdps`.`product_id` =".$request->input('product');
            }
            if ($request->input('speciality') != "")
            {
                $param .= " AND `doctors`.`speciality_id` =".$request->input('speciality');
            }
            if ($request->input('region') != "")
            {
                $param .= " AND `doctors`.`region_id` =".$request->input('region');
            }

            $products = \DB::table('products')
                ->join('group_products', 'group_products.product_id', '=', 'products.id')
                ->select(
                    'products.id',
                    'products.name')
                ->orderBy('name', 'asc');
            // Check role and filter data
            if (Auth::user()->role_id != 1) {
                $products = $products->select("id", "name")->where('group_id', Auth::user()->mr_group)->get();
            } else {
                $products = $products->select("id", "name")->get();
            }
            $selectProducts = $products->toJson();
            // $selectProducts = array();
            // foreach($products as $prd) {
            //     $selectProducts[$prd->id] = $prd->name;
            // }     
            
            $specialities = Speciality::orderBy('name', 'asc')->get();
            $selectSpecialities = array();
            foreach($specialities as $spec) {
                $selectSpecialities[$spec->id] = $spec->name;
            }

            $regions = Region::orderBy('name', 'asc')->get();
            $selectRegions = array();
            foreach($regions as $reg) {
                $selectRegions[$reg->id] = $reg->name;
            }

            $data = [];
            if ($param != ""){
                $data = DB::select(
                    'SELECT `product_potentials`.`name` AS product_potential, 
                        COUNT(`product_potentials`.`name`) AS `total`
                        FROM `doctors` 
                        JOIN `pdps` ON `pdps`.`doctor_id` = `doctors`.`id`
                        JOIN `product_potentials` ON `product_potentials`.`id` = `pdps`.`potential_id`
                        JOIN `group_products` ON `group_products`.`product_id` = `pdps`.`product_id`
                        WHERE 1 '.$group_param.$param.'
                        GROUP BY `product_potentials`.`name`
                    '
                );
            }

            return view ('reports.productprescription', 
                [
                    'data' => $data, 
                    'speciality' => $selectSpecialities, 
                    'product' => $selectProducts, 
                    'region' => $selectRegions                    
                ]
            );
        }
    }

    public function productperception(Request $request)
    {
        if (!Auth::check()){
            return redirect()->intended('/admin/login');
        } else {
            $param = '';
            $group_param = '';
            $group_product_join = ' ';
            if (Auth::user()->role_id != 1){
                $group_param = ' AND `group_products`.`group_id` = '.Auth::user()->mr_group;
                $group_product_join = ' JOIN `group_products` ON `group_products`.`product_id` = `pdps`.`product_id` ';
            }
            if ($request->input('product') != "")
            {
                $param .= " AND `products`.`id` =".$request->input('product');
            }
            if ($request->input('speciality') != "")
            {
                $param .= " AND `doctors`.`speciality_id` =".$request->input('speciality');
            }
            if ($request->input('region') != "")
            {
                $param .= " AND `doctors`.`region_id` =".$request->input('region');
            }

            $products = \DB::table('products')
                ->join('group_products', 'group_products.product_id', '=', 'products.id')
                ->select(
                    'products.id',
                    'products.name')
                ->orderBy('name', 'asc');
            // Check role and filter data
            if (Auth::user()->role_id != 1) {
                $products = $products->select("id", "name")->where('group_id', Auth::user()->mr_group)->get();
            } else {
                $products = $products->select("id", "name")->get();
            }
            $selectProducts = $products->toJson();
            // $selectProducts = array();
            // foreach($products as $prd) {
            //     $selectProducts[$prd->id] = $prd->name;
            // }     
            
            $specialities = Speciality::orderBy('name', 'asc')->get();
            $selectSpecialities = array();
            foreach($specialities as $spec) {
                $selectSpecialities[$spec->id] = $spec->name;
            }

            $regions = Region::orderBy('name', 'asc')->get();
            $selectRegions = array();
            foreach($regions as $reg) {
                $selectRegions[$reg->id] = $reg->name;
            }

            $data = [];
            if ($param != ""){
                $data = DB::select(
                    'SELECT `perceptions`.`name` AS perception, 
                        COUNT(`perceptions`.`name`) AS total
                        FROM `perceptions` 
                        JOIN `pdps` ON `pdps`.`perception_id` = `perceptions`.`id`
                        JOIN `products` ON `products`.`id` = `pdps`.`product_id`
                        '.$group_product_join.'
                        JOIN `doctors` ON `doctors`.`id` = `pdps`.`doctor_id`
                        WHERE 1 '.$group_param.$param.'
                        GROUP BY `perceptions`.`name`'
                );
            }
            
            // \Debugbar::info('');

            return view ('reports.productperception', 
                [
                    'data' => $data, 
                    'speciality' => $selectSpecialities, 
                    'product' => $selectProducts, 
                    'region' => $selectRegions,  
                ]
            );
        }
    }

    public function doctorstatus(Request $request)
    {
        if (!Auth::check()){
            return redirect()->intended('/admin/login');
        } else {
            $param = '';
            if ($request->input('product') != "")
            {
                $param .= " AND `products`.`id` =".$request->input('product');
            }
            if ($request->input('doctor') != "")
            {
                $param .= " AND `doctors`.`id` =".$request->input('doctor');
            }
            
            $products = \DB::table('products')
                ->join('group_products', 'group_products.product_id', '=', 'products.id')
                ->select(
                    'products.id',
                    'products.name')
                ->orderBy('name', 'asc');
            // Check role and filter data
            if (Auth::user()->role_id != 1) {
                $products = $products->select('id','name')->where('group_id', Auth::user()->mr_group)->get();
            } else {
                $products = $products->select('id','name')->get();
            }

            // $selectProducts = array();
            // foreach($products as $prd) {
            //     $selectProducts[$prd->id] = $prd->name;
            // }     
            
            $doctors = Doctor::select('id','name')->orderBy('name', 'asc')->get();
            // $selectDoctors = array();
            // foreach($doctors as $doc) {
            //     $selectDoctors[$doc->id] = $doc->name;
            // }
            $selectProducts = $products->toJson();
            $selectDoctors = $doctors->toJson();
            
            $data = [];
            if ($param != "") {
                $data = DB::select(
                    'SELECT 
                        `perceptions`.`name` AS `perception`, 
                        `product_potentials`.`name` AS `potential`,
                        `products`.`name` AS `product`
                    FROM `pdps` 
                    JOIN `products` ON `products`.`id` = `pdps`.`product_id`
                    JOIN `group_products` ON `group_products`.`product_id` = `products`.`id`
                    JOIN `doctors` ON `doctors`.`id` = `pdps`.`doctor_id`
                    JOIN `perceptions` ON `perceptions`.`id` = `pdps`.`perception_id`
                    JOIN `product_potentials` ON `product_potentials`.`id` = `pdps`.`potential_id`
                    WHERE 1 '.$param
                );
            }
            
            // \Debugbar::info('');

            return view ('reports.doctorstatus', 
                [
                    'data' => $data, 
                    'doctor' => $selectDoctors, 
                    'product' => $selectProducts,  
                ]
            );
        }
    }

    public function doctor(Request $request)
    {
        if (!Auth::check()){
            return redirect()->intended('/admin/login');
        } else {
            $param = '';
            $group_param = '';
            if (Auth::user()->role_id != 1){
                $group_param = ' AND `group_products`.`group_id` = '.Auth::user()->mr_group.'
                                AND `schedules`.`mr_id` IN (
                                SELECT id FROM users WHERE `mr_group` = '.Auth::user()->mr_group.')';
            }

            $arr = array(date("Y-m-d"), date("Y-m-d"));

            if ($request->input('date') != "")
            {
                $arr = explode(" - ", $request->input('date'));
                $param .= " AND `schedules`.`schedule_date` between '".$arr[0]."' AND '".$arr[1]."'";
                // $param .= " AND `schedules`.`schedule_date` ='".$request->input('date')."'";
            }

            if ($request->input('doctor') != "")
            {
                $param .= " AND `doctors`.`id` =".$request->input('doctor');
            }

            $doctors = Doctor::select("id", "name")->orderBy('name', 'asc')->get();
            $selectDoctors = $doctors->toJson();
            // $selectDoctors = array();
            // foreach($doctors as $doc) {
            //     $selectDoctors[$doc->id] = $doc->name;
            // }
            
            $investment = [];

            if ($param != ""){
                $investment = DB::select(
                    'SELECT `products`.`name` as `product`, 
                            SUM(`schedules`.`other_investment`) as other_investment, 
                            SUM(`schedules`.`sample_qty`) as sample_qty
                        FROM `potentials` 
                        JOIN `pdps` ON `pdps`.`potential_id` = `potentials`.`id` 
                        JOIN `products` ON `products`.`id` = `pdps`.`product_id` 
                        JOIN `group_products` ON `group_products`.`product_id` = `pdps`.`product_id` 
                        JOIN `schedules` ON `schedules`.`pdp_id` = `pdps`.`id`
                        JOIN `doctors` ON `doctors`.`id` = `pdps`.`doctor_id`
                        WHERE 1 '.$group_param.$param.'
                        GROUP BY `products`.`name` 
                        ORDER BY `products`.`name`'
                );
            }
            
            return view ('reports.doctor', 
                [
                    'investment' => $investment, 
                    'doctor' => $selectDoctors,  
                    'date' => $arr                   
                ]
            );
        }
    }
}
