<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use Mapper;

class MapController extends Controller
{
    //
    public function index($latlong){
        $arr = explode(",", $latlong);
        // \Debugbar::info($arr);
        
        return view('map.index', ['arr' => $arr]);
    }
}
