<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use App\CallPriority;
use App\Product;
use App\Speciality;
use App\Region;
use App\Doctor;
use DateTime;

class MobileReportController extends Controller
{
    //
    public function index()
    {
        return view('reports.index');
    }

    public function schedule(Request $request)
    {
        $param = '';
        $dateObj = null;
        if ($request->input('date') != "")
        {
            $dateObj = json_decode($request->input('date'));
            \Debugbar::info($request->input('date'));
            $param .= " AND `schedules`.`schedule_date` between '".$dateObj->start."' AND '".$dateObj->end."'";
        } 
        if ($request->input('mr') != "")
        {
            $param .= " AND `schedules`.`mr_id` =".$request->input('mr');
        }
        if ($request->input('callStatus') != "")
        {
            $param .= " AND `schedules`.`status` =".$request->input('callStatus');
        }
        if ($request->input('product') != "")
        {
            $param .= " AND `pdps`.`product_id` =".$request->input('product');
        }

        $selectCallStatus = array(
            '0' => 'Pre Call',
            '1' => 'Post Call'
        );

        $products = \DB::table('products')
            ->join('group_products', 'group_products.product_id', '=', 'products.id')
            ->where('group_id', $request->input('mr_group'))
            ->select(
                'products.id',
                'products.name')
            ->orderBy('name', 'asc');
            // ->get();
        // $selectProducts = array();
        // foreach($products as $prd) {
        //     $selectProducts[$prd->id] = $prd->name;
        // }
        $products = $products->select("id", "name")->get();
        $selectProducts = $products->toJson();

        $cols = DB::select("call getPotentialLists()");

        $priority = [];

        if ($param != ""){
            $priority = DB::select(
                'SELECT `products`.`name` as `Product`,'. $cols[0]->cols .' 
                    FROM `potentials` 
                    JOIN `pdps` ON `pdps`.`potential_id` = `potentials`.`id` 
                    JOIN `doctors` ON `doctors`.`id` = `pdps`.`doctor_id` 
                    JOIN `products` ON `products`.`id` = `pdps`.`product_id` 
                    JOIN `group_products` ON `group_products`.`product_id` = `products`.`id`
                    JOIN `schedules` ON `schedules`.`pdp_id` = `pdps`.`id`
                    WHERE `group_products`.`group_id` = '.$request->input('mr_group').'
                        AND `schedules`.`mr_id` IN (
                            SELECT id FROM users WHERE `mr_group` = '.$request->input('mr_group').'
                    )'.$param.'
                    GROUP BY `products`.`name` ORDER BY `products`.`name`'
            );
        }
        if ($dateObj == null){
            $date = new DateTime();
            $dString = '{"start":"'.$date->format('Y-m-d').'","end":"'.$date->format('Y-m-d').'"}';
            $dateObj = json_decode($dString);         
        }
        return view ('reports.schedule-mobile', 
            [
                'request' => $request,
                'priority' => $priority, 
                'callStatus' => $selectCallStatus, 
                'product' => $selectProducts,
                'date' => $dateObj
            ]
        );
    }

    public function productprescription(Request $request)
    {
        $param = '';
        if ($request->input('product') != "")
        {
            $param .= " AND `pdps`.`product_id` =".$request->input('product');
        }
        if ($request->input('speciality') != "")
        {
            $param .= " AND `doctors`.`speciality_id` =".$request->input('speciality');
        }

        $products = \DB::table('products')
            ->join('group_products', 'group_products.product_id', '=', 'products.id')
            ->where('group_id', $request->input('mr_group'))
            ->select(
                'products.id',
                'products.name')
            ->orderBy('name', 'asc');

        $products = $products->select("id", "name")->get();
        $selectProducts = $products->toJson();
            // ->get();
        // $selectProducts = array();
        // foreach($products as $prd) {
        //     $selectProducts[$prd->id] = $prd->name;
        // }     
        
        $specialities = Speciality::orderBy('name', 'asc')->get();
        $selectSpecialities = array();
        foreach($specialities as $spec) {
            $selectSpecialities[$spec->id] = $spec->name;
        }

        $data = [];

        if ($param != ""){
            $data = DB::select(
                'SELECT `product_potentials`.`name` AS product_potential, 
                    COUNT(`product_potentials`.`name`) AS `total`
                    FROM `doctors` 
                    JOIN `pdps` ON `pdps`.`doctor_id` = `doctors`.`id`
                    JOIN `product_potentials` ON `product_potentials`.`id` = `pdps`.`potential_id`
                    JOIN `group_products` ON `group_products`.`product_id` = `pdps`.`product_id`
                    WHERE 1 '.$param.'
                    GROUP BY `product_potentials`.`name`'
            );
        }

        return view ('reports.productprescription-mobile', 
            [
                'data' => $data, 
                'speciality' => $selectSpecialities, 
                'product' => $selectProducts, 
                                
            ]
        );
    }

    public function productperception(Request $request)
    {
        $param = '';
        if ($request->input('product') != "")
        {
            $param .= " AND `products`.`id` =".$request->input('product');
        }
        if ($request->input('speciality') != "")
        {
            $param .= " AND `doctors`.`speciality_id` =".$request->input('speciality');
        }
        if ($request->input('region') != "")
        {
            $param .= " AND `doctors`.`region_id` =".$request->input('region');
        }

        $products = \DB::table('products')
            ->join('group_products', 'group_products.product_id', '=', 'products.id')
            ->where('group_id', $request->input('mr_group'))
            ->select(
                'products.id',
                'products.name')
            ->orderBy('name', 'asc');

        $products = $products->select("id", "name")->get();
        $selectProducts = $products->toJson();

        // $selectProducts = array();
        // foreach($products as $prd) {
        //     $selectProducts[$prd->id] = $prd->name;
        // }     
        
        $specialities = Speciality::orderBy('name', 'asc')->get();
        $selectSpecialities = array();
        foreach($specialities as $spec) {
            $selectSpecialities[$spec->id] = $spec->name;
        }

        $data = [];

        if ($param != ""){
            $data = DB::select(
                'SELECT `perceptions`.`name` AS perception, 
                    COUNT(`perceptions`.`name`) AS total
                    FROM `perceptions` 
                    JOIN `pdps` ON `pdps`.`perception_id` = `perceptions`.`id`
                    JOIN `products` ON `products`.`id` = `pdps`.`product_id`
                    JOIN `doctors` ON `doctors`.`id` = `pdps`.`doctor_id`
                    WHERE 1 '.$param.'
                    GROUP BY `perceptions`.`name`'
            );    
        }
        
        // \Debugbar::info('');

        return view ('reports.productperception-mobile', 
            [
                'data' => $data, 
                'speciality' => $selectSpecialities, 
                'product' => $selectProducts, 
                // 'region' => $selectRegions,  
            ]
        );
    }

    public function doctorstatus(Request $request)
    {
        $param = '';
        if ($request->input('product') != "")
        {
            $param .= " AND `products`.`id` =".$request->input('product');
        }
        if ($request->input('doctor') != "")
        {
            $param .= " AND `doctors`.`id` =".$request->input('doctor');
        }
        
        $products = \DB::table('products')
            ->join('group_products', 'group_products.product_id', '=', 'products.id')
            ->where('group_id', $request->input('mr_group'))
            ->select(
                'products.id',
                'products.name')
            ->orderBy('name', 'asc');
            // ->get();

        $products = $products->select('id','name')->get();

        $doctors = Doctor::select('id','name')->orderBy('name', 'asc')->get();
        $selectProducts = $products->toJson();
        $selectDoctors = $doctors->toJson();
        // $selectProducts = array();
        // foreach($products as $prd) {
        //     $selectProducts[$prd->id] = $prd->name;
        // }     
        
        // $doctors = Doctor::orderBy('name', 'asc')->get();
        // $selectDoctors = array();
        // foreach($doctors as $doc) {
        //     $selectDoctors[$doc->id] = $doc->name;
        // }

        $data = [];
        if ($param != "") {
            $data = DB::select(
                'SELECT 
                    `perceptions`.`name` AS `perception`, 
                    `product_potentials`.`name` AS `potential`,
                    `products`.`name` AS `product`
                FROM `pdps` 
                JOIN `products` ON `products`.`id` = `pdps`.`product_id`
                JOIN `group_products` ON `group_products`.`product_id` = `products`.`id`
                JOIN `doctors` ON `doctors`.`id` = `pdps`.`doctor_id`
                JOIN `perceptions` ON `perceptions`.`id` = `pdps`.`perception_id`
                JOIN `product_potentials` ON `product_potentials`.`id` = `pdps`.`potential_id`
                WHERE 1 '.$param
            );
        }
        // \Debugbar::info('');
        return view ('reports.doctorstatus-mobile', 
            [
                'data' => $data, 
                'doctor' => $selectDoctors, 
                'product' => $selectProducts,  
            ]
        );
    }

    public function doctor(Request $request)
    {
        $param = '';
        if ($request->input('doctor') != "")
        {
            $param .= " AND `doctors`.`id` =".$request->input('doctor');
        }

        // $doctors = Doctor::orderBy('name', 'asc')->get();
        $doctors = Doctor::select("id", "name")->orderBy('name', 'asc')->get();
        $selectDoctors = $doctors->toJson();

        // $selectDoctors = array();
        // foreach($doctors as $doc) {
        //     $selectDoctors[$doc->id] = $doc->name;
        // }

        $investment = [];

        if ($param != "") {
            $investment = DB::select(
                'SELECT `products`.`name` as `product`, 
                        SUM(`schedules`.`other_investment`) as other_investment, 
                        SUM(`schedules`.`sample_qty`) as sample_qty
                    FROM `potentials` 
                    JOIN `pdps` ON `pdps`.`potential_id` = `potentials`.`id` 
                    JOIN `products` ON `products`.`id` = `pdps`.`product_id` 
                    JOIN `group_products` ON `group_products`.`product_id` = `pdps`.`product_id` 
                    JOIN `schedules` ON `schedules`.`pdp_id` = `pdps`.`id`
                    JOIN `doctors` ON `doctors`.`id` = `pdps`.`doctor_id`
                    WHERE `group_products`.`group_id` = '.$request->input('mr_group').'
                        AND `schedules`.`mr_id` IN (
                            SELECT id FROM users WHERE `mr_group` = '.$request->input('mr_group').'
                    )'.$param.'
                    GROUP BY `products`.`name` 
                    ORDER BY `products`.`name`'
            );
        }
        
        return view ('reports.doctor-mobile', 
            [
                'investment' => $investment, 
                'doctor' => $selectDoctors,                    
            ]
        );
    }
}
