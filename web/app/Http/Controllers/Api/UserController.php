<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use Helpers;
    
    public function getUser(Request $request)
    {
        $email = $request->input('email');
        $user = User::where('email', $email)->first();
        return $this->response->array($user->toArray());
    }
}
