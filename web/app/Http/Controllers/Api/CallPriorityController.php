<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use App\CallPriority;

class CallPriorityController extends Controller
{
    use Helpers;
    
    public function getCallPriority(Request $request)
    {
        $callprority = CallPriority::all();
        return $this->response->array($callprority->toArray());
    }
}
