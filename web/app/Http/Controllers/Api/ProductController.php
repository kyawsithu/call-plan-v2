<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use App\Product;
use App\Pdp;

class ProductController extends Controller
{
    use Helpers;

    public function getProducts(Request $request)
    {
        $products = Product::all();
        return $this->response->array($products->toArray());
    }

    public function getPDPs(Request $request)
    {
        $doctor_id = $request->input('doctor_id');
        $products = \DB::table('products')
        ->join('pdps', 'pdps.product_id', '=', 'products.id')
        ->join('perceptions', 'perceptions.id', '=', 'pdps.perception_id')
        ->join('product_potentials', 'product_potentials.id', "=", 'pdps.potential_id')
        ->where('pdps.doctor_id', '=', $doctor_id)
        ->select(
            'products.*',
            'perceptions.name as perception',
            'pdps.id as pdp_id',
            'product_potentials.name as potential')
        ->get();
        return $this->response->array($products->toArray());
    }

    public function editPdp(Request $request)
    {
        $pdp = Pdp::find($request->input('id'));
        $pdp->perception_id = $request->input('perceptionId'); // status = 1 Completed
        $pdp->potential_id = $request->input('potentialId');
        $pdp->save();
        return $this->response->noContent();
    }
}
