<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Doctor;
use Dingo\Api\Routing\Helpers;

class DoctorController extends Controller
{
    use Helpers;

    public function getDoctors(Request $request)
    {
        // $doctors = Doctor::all();

        $doctors = \DB::table('doctors')
        ->join('regions', 'regions.id', '=', 'doctors.region_id')
        ->join('specialities', 'specialities.id', '=', 'doctors.region_id')
        ->join('potentials', 'potentials.id', "=", 'doctors.potential_id')
        ->join('designations', 'designations.id', "=", 'doctors.designation_id')
        ->join('profiles', 'profiles.id', "=", 'doctors.profile_id')
        ->select(
            'doctors.*', 
            'specialities.name as speciality', 
            'regions.name as region',
            'potentials.name as potential',
            'designations.name as designation',
            'profiles.name as profile')
        ->get();

        return $this->response->array($doctors->toArray());
    }
}
