<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Schedule;
use App\Pdp;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Log;

class ScheduleController extends Controller
{
    //
    use Helpers;
    
    public function scheduleAdd(Request $request)
    {
        Log::info('New schedule: '.$request);

        $schedule = new Schedule;
        $schedule->mr_id = $request->input('mr_id');
        $schedule->pdp_id = $request->input('pdp_id');
        $schedule->sample_qty = $request->input('sample_qty');
        $schedule->other_investment = $request->input('other_investment');
        $schedule->latlong = $request->input('latlong');
        $schedule->status = $request->input('status');
        $schedule->schedule_date = $request->input('schedule_date');
        $schedule->call_priority_id = $request->input('call_priority_id');
        $temp = $schedule->save();
        
        Log::info('New schedule: '.$temp);

        return $this->response->array($temp);
    }

    public function getOriginalSchedules(Request $request)
    {
        $schedules = Schedule::all();
        return $this->response->array($schedules->toArray());
    }

    public function getSchedules(Request $request)
    {
        $schedules = \DB::table('schedules')
            ->join('pdps', 'pdps.id', '=', 'schedules.pdp_id')
            ->join('potentials', 'potentials.id', '=', 'pdps.potential_id')
            ->join('perceptions', 'perceptions.id', '=', 'pdps.perception_id')
            ->join('doctors', 'doctors.id', '=', 'pdps.doctor_id')
            ->join('products', 'products.id', '=', 'pdps.product_id')
            ->join('product_potentials', 'product_potentials.id', '=', 'pdps.potential_id')
            ->select(
                'schedules.*',
                'perceptions.name as perception',
                'potentials.name as potential',
                'doctors.name as doctor', 
                'product_potentials.name as potential', 
                'products.name as product')
            ->get();

        return $this->response->array($schedules->toArray());
    }

    public function getSchedulesByMr(Request $request)
    {
        $schedules = \DB::table('schedules')
            ->join('pdps', 'pdps.id', '=', 'schedules.pdp_id')
            // ->join('potentials', 'potentials.id', '=', 'pdps.potential_id')
            ->join('perceptions', 'perceptions.id', '=', 'pdps.perception_id')
            ->join('doctors', 'doctors.id', '=', 'pdps.doctor_id')
            ->join('products', 'products.id', '=', 'pdps.product_id')
            ->join('call_priorities', 'call_priorities.id', "=", 'schedules.call_priority_id')
            ->join('product_potentials', 'product_potentials.id', '=', 'pdps.potential_id')
            ->where('mr_id', $request->input('mrid'))
            ->select(
                'schedules.*',
                'call_priorities.name as call_priority',
                'perceptions.name as perception',
                // 'potentials.name as potential',
                'doctors.name as doctor', 
                'product_potentials.name as potential', 
                'products.name as product')
            ->get();

        return $this->response->array($schedules->toArray());
    }

    public function scheduleComplete(Request $request)
    {
        Log::info('Complete schedule: '.$request);

        $schedule = Schedule::find($request->input('id'));
        $schedule->latlong = $request->input('latlong');
        $schedule->status = 1; // status = 1 Completed
        $schedule->save();
        return $this->response->noContent();
    }

    public function scheduleDelete(Request $request)
    {
        $schedule = Schedule::find($request->input('id'));
        $schedule->delete();
        return $this->response->noContent();
    }
}
