<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\PotentialTransformer;
use Dingo\Api\Routing\Helpers;
use App\Perception;

class PerceptionController extends Controller
{
    use Helpers;
    
    public function getPerceptions()
    {
        $perceptions = Perception::all();
        return $this->response->array($perceptions->toArray());
    }
}