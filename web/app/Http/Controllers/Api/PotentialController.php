<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\PotentialTransformer;
use Dingo\Api\Routing\Helpers;
use App\Potential;

class PotentialController extends Controller
{
    use Helpers;
    
    public function getPotentials()
    {
        $potentials = Potential::all();
        // return $this->response->collection($regions, new PotentialTransformer());
        return $this->response->array($potentials->toArray());
    }
}