<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\PotentialTransformer;
use Dingo\Api\Routing\Helpers;
use App\ProductPotential;

class PrescriptionController extends Controller
{
    use Helpers;
    
    public function getPrescriptions()
    {
        $potentials = ProductPotential::all();
        return $this->response->array($potentials->toArray());
    }
}