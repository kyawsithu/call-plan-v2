<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\RegionTransformer;
use Dingo\Api\Routing\Helpers;
use App\Region;

class RegionController extends Controller
{
    use Helpers;
    
    public function getRegions()
    {
        $regions = Region::all();
        return $this->response->collection($regions, new RegionTransformer());
    }
}