<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\DesignationTransformer;
use Dingo\Api\Routing\Helpers;
use App\Designation;

class DesignationController extends Controller
{
    use Helpers;
    
    public function getDesignations()
    {
        // $designations = Designation::all();
        $designations = Designation::paginate(2);
        return $this->response->paginator($designations, new DesignationTransformer());
    }
}