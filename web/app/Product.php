<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    public function groups()
    {
        return $this->belongsToMany('App\Group','Pivot table');
    }
}
