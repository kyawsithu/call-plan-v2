<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class Calendar extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(Request $request)
    {
        // \Debugbar::info($request->input('mr'));
        $mrs = User::orderBy('name', 'asc')->where('mr_group', Auth::user()->mr_group)->get();
        $selectMrs = array();
        foreach($mrs as $mr) {
            $selectMrs[$mr->id] = $mr->name;
        }

        // $schedules = \App\Schedule::all();
        $schedules = \DB::table('schedules')
        ->join('pdps', 'pdps.id', '=', 'schedules.pdp_id')
        ->join('potentials', 'potentials.id', '=', 'pdps.potential_id')
        ->join('perceptions', 'perceptions.id', '=', 'pdps.perception_id')
        ->join('doctors', 'doctors.id', '=', 'pdps.doctor_id')
        ->join('products', 'products.id', '=', 'pdps.product_id')
        ->join('product_potentials', 'product_potentials.id', '=', 'pdps.potential_id')
        ->select(
            'schedules.*',
            'perceptions.name as perception',
            'potentials.name as potential',
            'doctors.name as doctor', 
            'product_potentials.name as potential', 
            'products.name as product');
        // ->get();

        if ($request->input('mr') != ""){
            $schedules->where("schedules.mr_id", $request->input('mr'));
        }

        $data = $schedules->get();
        $events = [];
        // \Debugbar::info($schedules);
        foreach($data as $item){
            $event = array(
                'title' => $item->doctor." - ".$item->product.", ".$item->potential.", ".$item->perception . " | Actual Complete Date: ".$item->updated_at,
                // 'description' => $item->doctor,
                'start' => $item->schedule_date,
                'end' => $item->schedule_date,
                'color' => ($item->status == 1 ? '#00e600' : '#33ccff'),
                'coordinate' => $item->latlong
            );
            array_push($events, $event);
        }
        
        return view ('calendar', [ 
            'events' => $events, 
            'mr' => $selectMrs 
        ]);
    }
}
