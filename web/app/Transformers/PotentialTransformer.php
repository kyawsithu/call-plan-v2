<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Potential;

class PotentialTransformer extends TransformerAbstract
{
    public function transform(Potential $obj)
    {
        return [
            'id' => $obj->id,
            'name' => $obj->name,
            'created_at' => date('Y-m-d', strtotime($obj->created_at))
        ];
    }
}
