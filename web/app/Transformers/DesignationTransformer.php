<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Designation;

class DesignationTransformer extends TransformerAbstract
{
    public function transform(Designation $obj)
    {
        return [
            'id' => $obj->id,
            'name' => $obj->name,
            'created_at' => date('Y-m-d', strtotime($obj->created_at))
        ];
    }
}
