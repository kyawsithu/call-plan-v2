<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Region;

class RegionTransformer extends TransformerAbstract
{
    public function transform(Region $obj)
    {
        return [
            'id' => $obj->id,
            'name' => $obj->name,
            'created_at' => date('Y-m-d', strtotime($obj->created_at))
        ];
    }
}
