<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $obj)
    {
        return [
            'id' => $obj->id,
            'role_id' => $obj->role_id,
            'name' => $obj->name,
            'email' => $obj->email,
            'avatar' => $obj->avatar,
            'region_id' => $obj->region_id,
            'created_at' => date('Y-m-d', strtotime($obj->created_at)),
            'updated_at' => date('Y-m-d', strtotime($obj->updated_at))
        ];
    }
}
