DELIMITER //
DROP TRIGGER IF EXISTS OnProductInsert //

CREATE TRIGGER OnProductInsert AFTER INSERT ON `products` 
FOR EACH ROW BEGIN
  DECLARE v_product_id, v_perception_id, v_product_potential_id INT;
  SET v_product_id = NEW.id;
  SET v_perception_id = (SELECT id FROM perceptions WHERE name = 'Unaware');
  SET v_product_potential_id = (SELECT id FROM product_potentials WHERE name = 'NP (Non Prescriber)');  
  
  INSERT INTO pdps (doctor_id, perception_id, potential_id, product_id, created_at, updated_at) 
  	SELECT id, v_perception_id, v_product_potential_id, v_product_id, NOW(), NOW() FROM doctors;
END//

DELIMITER ;


DELIMITER //
DROP TRIGGER IF EXISTS OnDoctorInsert //

CREATE TRIGGER OnDoctorInsert AFTER INSERT ON `doctors` 
FOR EACH ROW BEGIN
  DECLARE v_doctor_id, v_perception_id, v_product_potential_id INT;
  SET v_doctor_id = NEW.id;
  SET v_perception_id = (SELECT id FROM perceptions WHERE name = 'Unaware');
  SET v_product_potential_id = (SELECT id FROM product_potentials WHERE name = 'NP (Non Prescriber)');  
  
  INSERT INTO pdps (doctor_id, perception_id, potential_id, product_id, created_at, updated_at) 
  	SELECT v_doctor_id, v_perception_id, v_product_potential_id, id, NOW(), NOW() FROM products;
END//

DELIMITER ;


