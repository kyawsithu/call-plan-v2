-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 21, 2017 at 04:01 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.1.10-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homestead`
--

-- DELIMITER $$
-- --
-- -- Procedures
-- --
-- CREATE DEFINER=`homestead`@`%` PROCEDURE `getPotentialLists` ()  NO SQL
-- SELECT GROUP_CONCAT(DISTINCT CONCAT( 'IFNULL(COUNT(CASE WHEN potentials.name = "', potentials.name, '" THEN potentials.name END),0) AS `', potentials.name, '`' ) ) AS cols FROM potentials$$

-- DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `call_priorities`
--

CREATE TABLE `call_priorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `call_priorities`
--

INSERT INTO `call_priorities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Full Call', '2017-11-16 15:23:41', '2017-11-16 15:23:41'),
(2, 'Half Call', '2017-11-16 15:23:51', '2017-11-16 15:23:51');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(26, 3, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, NULL, 1),
(27, 3, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, NULL, 2),
(28, 3, 'email', 'text', 'email', 1, 1, 1, 1, 1, 1, NULL, 3),
(29, 3, 'password', 'password', 'password', 1, 0, 0, 1, 1, 0, NULL, 4),
(30, 3, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 11),
(31, 3, 'remember_token', 'text', 'remember_token', 0, 0, 0, 0, 0, 0, NULL, 5),
(32, 3, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, NULL, 6),
(33, 3, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, NULL, 7),
(34, 3, 'avatar', 'image', 'avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(35, 5, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(36, 5, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
(37, 5, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(38, 5, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(46, 6, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(47, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(48, 6, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(49, 6, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(50, 6, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(53, 3, 'role_id', 'text', 'role_id', 0, 1, 1, 1, 1, 1, NULL, 9),
(54, 8, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(55, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(56, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 3),
(57, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(58, 9, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(59, 9, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(60, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 3),
(61, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(66, 11, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(67, 11, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(68, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 3),
(69, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(74, 13, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(75, 13, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(76, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 3),
(77, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(78, 14, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(79, 14, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(80, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 3),
(81, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(83, 15, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(84, 15, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(85, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 3),
(86, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(87, 16, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(88, 16, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(89, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 3),
(90, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(91, 17, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(92, 17, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(98, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 14),
(99, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 9),
(100, 17, 'doctor_belongsto_designation_relationship', 'relationship', 'Designation', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Designation\",\"table\":\"designations\",\"type\":\"belongsTo\",\"column\":\"designation_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 10),
(101, 17, 'designation_id', 'text', 'Designation Id', 1, 1, 1, 1, 1, 1, NULL, 3),
(102, 17, 'potential_id', 'text', 'Potential Id', 1, 1, 1, 1, 1, 1, NULL, 4),
(103, 17, 'profile_id', 'text', 'Profile Id', 1, 1, 1, 1, 1, 1, NULL, 5),
(104, 17, 'speciality_id', 'text', 'Speciality Id', 1, 1, 1, 1, 1, 1, NULL, 8),
(105, 17, 'doctor_belongsto_profile_relationship', 'relationship', 'Profile', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Profile\",\"table\":\"profiles\",\"type\":\"belongsTo\",\"column\":\"profile_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 11),
(106, 17, 'doctor_belongsto_potential_relationship', 'relationship', 'Potential', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Potential\",\"table\":\"potentials\",\"type\":\"belongsTo\",\"column\":\"potential_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 12),
(107, 17, 'doctor_belongsto_speciality_relationship', 'relationship', 'Speciality', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Speciality\",\"table\":\"specialities\",\"type\":\"belongsTo\",\"column\":\"speciality_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 13),
(108, 18, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(109, 18, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(110, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 3),
(111, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(112, 3, 'user_belongsto_region_relationship', 'relationship', 'Region', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Region\",\"table\":\"regions\",\"type\":\"belongsTo\",\"column\":\"region_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 10),
(113, 17, 'doctor_belongsto_region_relationship', 'relationship', 'Region', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Region\",\"table\":\"regions\",\"type\":\"belongsTo\",\"column\":\"region_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 6),
(114, 17, 'region_id', 'checkbox', 'Region Id', 1, 1, 1, 1, 1, 1, NULL, 7),
(115, 3, 'region_id', 'text', 'Region Id', 0, 1, 1, 1, 1, 1, NULL, 10),
(116, 19, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(117, 19, 'group', 'text', 'Group', 1, 1, 1, 1, 1, 1, NULL, 2),
(118, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 3),
(119, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(120, 3, 'user_belongsto_mr_group_relationship', 'relationship', 'MR Group', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Group\",\"table\":\"groups\",\"type\":\"belongsTo\",\"column\":\"mr_group\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 12),
(121, 3, 'mr_group', 'text', 'MR Group', 0, 1, 1, 1, 1, 1, NULL, 11),
(126, 21, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(127, 21, 'doctor_id', 'text', 'Doctor Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(128, 21, 'perception_id', 'text', 'Perception Id', 0, 1, 1, 1, 1, 1, NULL, 3),
(129, 21, 'product_id', 'text', 'Product Id', 0, 1, 1, 1, 1, 1, NULL, 4),
(130, 21, 'potential_id', 'text', 'Potential Id', 0, 1, 1, 1, 1, 1, NULL, 5),
(131, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 11),
(132, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(133, 21, 'pdp_belongsto_doctor_relationship', 'relationship', 'Doctor', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Doctor\",\"table\":\"doctors\",\"type\":\"belongsTo\",\"column\":\"doctor_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 7),
(134, 21, 'pdp_belongsto_perception_relationship', 'relationship', 'Perception', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Perception\",\"table\":\"perceptions\",\"type\":\"belongsTo\",\"column\":\"perception_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 8),
(135, 21, 'pdp_belongsto_potential_relationship', 'relationship', 'Prescription', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ProductPotential\",\"table\":\"product_potentials\",\"type\":\"belongsTo\",\"column\":\"potential_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 9),
(136, 21, 'pdp_belongsto_product_relationship', 'relationship', 'Product', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"product_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 10),
(137, 22, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(138, 22, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(139, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 3),
(140, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(141, 25, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(142, 25, 'pdp_id', 'text', 'Pdp Id', 1, 1, 1, 1, 1, 1, NULL, 2),
(143, 25, 'mr_id', 'text', 'Mr Id', 1, 1, 1, 1, 1, 1, NULL, 3),
(144, 25, 'other_investment', 'text', 'Other Investment', 0, 1, 1, 1, 1, 1, NULL, 4),
(145, 25, 'sample_qty', 'text', 'Sample Qty', 0, 1, 1, 1, 1, 1, NULL, 5),
(146, 25, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 1, NULL, 8),
(147, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 9),
(148, 25, 'schedule_belongsto_user_relationship', 'relationship', 'MR', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"mr_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 6),
(149, 25, 'schedule_belongsto_pdp_relationship', 'relationship', 'PDP', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\pdp\",\"table\":\"pdps\",\"type\":\"belongsTo\",\"column\":\"pdp_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 7),
(150, 26, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(151, 26, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(152, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 4),
(153, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(159, 26, 'group_belongstomany_product_relationship', 'relationship', 'Products', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"group_products\",\"pivot\":\"1\"}', 3),
(160, 25, 'schedule_date', 'date', 'Schedule Date', 0, 1, 1, 1, 1, 1, NULL, 8),
(161, 25, 'call_priority_id', 'text', 'Call Priority', 0, 1, 1, 1, 1, 1, NULL, 9),
(162, 25, 'schedule_belongsto_call_priority_relationship', 'relationship', 'Call Priority', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\CallPriority\",\"table\":\"call_priorities\",\"type\":\"belongsTo\",\"column\":\"call_priority_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"call_priorities\",\"pivot\":\"0\"}', 10),
(163, 25, 'latlong', 'text', 'Latlong', 0, 1, 1, 1, 1, 1, NULL, 10),
(164, 25, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, NULL, 11);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`) VALUES
(3, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '2017-11-13 17:40:47', '2017-11-16 14:30:28'),
(5, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, '2017-11-13 17:40:47', '2017-11-13 17:40:47'),
(6, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, '2017-11-13 17:40:47', '2017-11-13 17:40:47'),
(8, 'designations', 'designations', 'Designation', 'Designations', 'voyager-group', 'App\\Designation', NULL, NULL, NULL, 1, 0, '2017-11-13 17:49:30', '2017-11-13 17:49:30'),
(9, 'perceptions', 'perceptions', 'Perception', 'Perceptions', 'voyager-anchor', 'App\\Perception', NULL, NULL, NULL, 1, 0, '2017-11-13 17:51:17', '2017-11-13 17:51:17'),
(11, 'potentials', 'potentials', 'Potential', 'Potentials', 'voyager-activity', 'App\\Potential', NULL, NULL, NULL, 1, 0, '2017-11-16 14:19:56', '2017-11-16 14:19:56'),
(13, 'call_priorities', 'call-priorities', 'Call Priority', 'Call Priorities', 'voyager-diamond', 'App\\CallPriority', NULL, NULL, NULL, 1, 0, '2017-11-16 14:22:28', '2017-11-16 14:22:28'),
(14, 'products', 'products', 'Product', 'Products', 'voyager-logbook', 'App\\Product', NULL, NULL, NULL, 1, 0, '2017-11-16 14:28:17', '2017-11-26 14:59:09'),
(15, 'specialities', 'specialities', 'Speciality', 'Specialities', 'voyager-lifebuoy', 'App\\Speciality', NULL, NULL, NULL, 1, 0, '2017-11-16 14:33:23', '2017-11-16 14:33:23'),
(16, 'profiles', 'profiles', 'Profile', 'Profiles', NULL, 'App\\Profile', NULL, NULL, NULL, 1, 0, '2017-11-18 15:52:02', '2017-11-18 15:52:02'),
(17, 'doctors', 'doctors', 'Doctor', 'Doctors', NULL, 'App\\Doctor', NULL, NULL, NULL, 1, 0, '2017-11-18 15:57:42', '2017-11-18 15:57:42'),
(18, 'regions', 'regions', 'Region', 'Regions', 'voyager-world', 'App\\Region', NULL, NULL, NULL, 1, 0, '2017-11-18 16:37:07', '2017-11-18 16:37:07'),
(19, 'mr_group', 'mr-group', 'Mr Group', 'Mr Groups', 'voyager-people', 'App\\MrGroup', NULL, NULL, NULL, 1, 0, '2017-11-25 13:27:34', '2017-11-25 13:27:34'),
(21, 'pdps', 'pdps', 'Pdp', 'Pdps', 'voyager-campfire', 'App\\Pdp', NULL, NULL, NULL, 1, 0, '2017-11-26 11:29:15', '2017-11-26 11:29:15'),
(22, 'product_potentials', 'product-potentials', 'Prescription', 'Prescriptions', 'voyager-cannon', 'App\\ProductPotential', NULL, NULL, NULL, 1, 0, '2017-11-26 11:43:22', '2017-12-16 15:03:01'),
(25, 'schedules', 'schedules', 'Schedule', 'Schedules', 'voyager-alarm-clock', 'App\\Schedule', NULL, NULL, NULL, 1, 0, '2017-11-29 14:50:16', '2017-12-17 07:01:29'),
(26, 'groups', 'groups', 'Group', 'Groups', 'voyager-people', 'App\\Group', NULL, NULL, NULL, 1, 0, '2017-12-16 15:40:37', '2017-12-16 15:40:37');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Professor', '2017-11-13 17:50:03', '2017-11-13 17:50:03'),
(2, 'Assitant Surgeon', '2017-11-16 15:28:01', '2017-11-16 15:28:01'),
(3, 'Associate Professor', '2017-11-16 15:28:17', '2017-11-16 15:28:17'),
(4, 'Consultant', '2017-11-16 15:28:31', '2017-11-16 15:28:31'),
(5, 'First Assistant', '2017-11-16 15:28:52', '2017-11-16 15:28:52');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `designation_id` int(11) NOT NULL,
  `potential_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `speciality_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `name`, `designation_id`, `potential_id`, `profile_id`, `region_id`, `speciality_id`, `created_at`, `updated_at`) VALUES
(1, 'Aye Aye Myint, Daw', 1, 1, 2, 1, 5, '2017-11-18 16:35:00', '2017-11-26 15:11:19'),
(2, 'May Thu, Daw', 4, 1, 1, 1, 4, '2017-11-26 05:44:32', '2017-11-26 05:44:32'),
(3, 'Zaw Wai Soe, U', 1, 1, 1, 1, 2, '2017-11-26 05:45:03', '2017-11-26 05:45:03'),
(4, 'Theingi Win, Daw', 3, 3, 4, 1, 5, '2017-11-26 15:14:35', '2017-11-26 15:14:35'),
(5, 'Tin Tin Aye Daw', 1, 1, 1, 1, 1, '2017-12-16 10:11:14', '2017-12-16 10:11:14');

--
-- Triggers `doctors`
--
DELIMITER $$
CREATE TRIGGER `OnDoctorInsert` AFTER INSERT ON `doctors` FOR EACH ROW BEGIN
  DECLARE v_doctor_id, v_perception_id, v_product_potential_id INT;
  SET v_doctor_id = NEW.id;
  SET v_perception_id = (SELECT id FROM perceptions WHERE name = 'Unaware');
  SET v_product_potential_id = (SELECT id FROM product_potentials WHERE name = 'NP (Non Prescriber)');  
  
  INSERT INTO pdps (doctor_id, perception_id, potential_id, product_id, created_at, updated_at) 
  	SELECT v_doctor_id, v_perception_id, v_product_potential_id, id, NOW(), NOW() FROM products;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Group A', '2017-12-16 16:32:56', '2017-12-16 16:32:56'),
(2, 'Group B', '2017-12-16 16:50:30', '2017-12-16 16:50:30');

-- --------------------------------------------------------

--
-- Table structure for table `group_products`
--

CREATE TABLE `group_products` (
  `group_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `group_products`
--

INSERT INTO `group_products` (`group_id`, `product_id`) VALUES
(1, 1),
(1, 2),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2017-11-13 17:40:47', '2017-11-13 17:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2017-11-13 17:40:47', '2017-11-13 17:40:47', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 3, '2017-11-13 17:40:47', '2017-11-25 13:44:49', 'voyager.media.index', NULL),
(3, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 4, '2017-11-13 17:40:47', '2017-11-25 13:44:49', 'voyager.posts.index', NULL),
(4, 1, 'Users', '', '_self', 'voyager-person', NULL, 20, 2, '2017-11-13 17:40:47', '2017-11-16 14:42:35', 'voyager.users.index', NULL),
(5, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 6, '2017-11-13 17:40:47', '2017-11-25 13:44:49', 'voyager.categories.index', NULL),
(6, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 5, '2017-11-13 17:40:47', '2017-11-25 13:44:49', 'voyager.pages.index', NULL),
(7, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 20, 1, '2017-11-13 17:40:47', '2017-11-16 14:42:31', 'voyager.roles.index', NULL),
(8, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 7, '2017-11-13 17:40:47', '2017-12-16 16:34:12', NULL, NULL),
(9, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 8, 1, '2017-11-13 17:40:47', '2017-11-16 14:36:20', 'voyager.menus.index', NULL),
(10, 1, 'Database', '', '_self', 'voyager-data', NULL, 8, 2, '2017-11-13 17:40:47', '2017-11-16 14:36:20', 'voyager.database.index', NULL),
(11, 1, 'Compass', '/admin/compass', '_self', 'voyager-compass', NULL, 8, 3, '2017-11-13 17:40:47', '2017-11-16 14:36:20', NULL, NULL),
(12, 1, 'Hooks', '/admin/hooks', '_self', 'voyager-hook', NULL, 8, 4, '2017-11-13 17:40:47', '2017-11-16 14:36:20', NULL, NULL),
(13, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 12, '2017-11-13 17:40:47', '2017-12-16 16:34:12', 'voyager.settings.index', NULL),
(14, 1, 'Designations', '/admin/designations', '_self', 'voyager-group', NULL, 24, 3, '2017-11-13 17:49:30', '2017-11-26 15:03:32', NULL, NULL),
(15, 1, 'Perceptions', '/admin/perceptions', '_self', 'voyager-anchor', NULL, 24, 1, '2017-11-13 17:51:18', '2017-11-19 06:50:53', NULL, NULL),
(16, 1, 'Call Priorities', '/admin/call-priorities', '_self', 'voyager-diamond', '#000000', 24, 4, '2017-11-16 14:17:21', '2017-12-16 16:34:05', NULL, ''),
(17, 1, 'Potentials', '/admin/potentials', '_self', 'voyager-activity', NULL, 24, 5, '2017-11-16 14:19:56', '2017-12-20 14:44:41', NULL, NULL),
(18, 1, 'Products', '/admin/products', '_self', 'voyager-logbook', NULL, NULL, 9, '2017-11-16 14:28:18', '2017-12-16 16:34:12', NULL, NULL),
(19, 1, 'Specialities', '/admin/specialities', '_self', 'voyager-lifebuoy', NULL, 24, 6, '2017-11-16 14:33:23', '2017-12-20 14:44:41', NULL, NULL),
(20, 1, 'User Management', '', '_self', 'voyager-people', '#000000', NULL, 2, '2017-11-16 14:36:04', '2017-11-16 14:36:31', NULL, ''),
(21, 1, 'Profiles', '/admin/profiles', '_self', 'voyager-person', '#fff894', 24, 7, '2017-11-18 15:52:02', '2017-12-20 14:44:41', NULL, ''),
(22, 1, 'Doctors', '/admin/doctors', '_self', 'voyager-study', '#000000', NULL, 8, '2017-11-18 15:57:43', '2017-12-16 16:34:12', NULL, ''),
(23, 1, 'Regions', '/admin/regions', '_self', 'voyager-world', NULL, 24, 8, '2017-11-18 16:37:08', '2017-12-20 14:44:41', NULL, NULL),
(24, 1, 'Master Data', '', '_self', 'voyager-categories', '#000000', NULL, 11, '2017-11-19 06:49:40', '2017-12-16 16:34:12', NULL, ''),
(27, 1, 'Pdps', '/admin/pdps', '_self', 'voyager-campfire', NULL, NULL, 10, '2017-11-26 11:29:15', '2017-12-16 16:34:12', NULL, NULL),
(28, 1, 'Prescriptions', '/admin/product-potentials', '_self', 'voyager-cannon', '#000000', 24, 2, '2017-11-26 11:43:22', '2017-12-16 15:03:55', NULL, ''),
(29, 1, 'Schedules', '/admin/schedules', '_self', 'voyager-alarm-clock', NULL, NULL, 13, '2017-11-29 14:50:17', '2017-12-16 16:34:12', NULL, NULL),
(30, 1, 'Groups', '/admin/groups', '_self', 'voyager-people', NULL, 20, 3, '2017-12-16 15:40:37', '2017-12-16 18:37:19', NULL, NULL),
(31, 1, 'Group Products', '/admin/group-products', '_self', 'voyager-kraken', NULL, NULL, 14, '2017-12-16 16:07:43', '2017-12-16 16:34:12', NULL, NULL),
(34, 1, 'Reports', '', '_self', 'voyager-receipt', '#000000', NULL, 15, '2017-12-20 14:17:45', '2017-12-20 14:44:44', NULL, ''),
(35, 1, 'Schedule Reports', '/admin/reports/schedule', '_self', 'voyager-alarm-clock', '#000000', 34, 1, '2017-12-20 14:19:38', '2017-12-20 14:19:54', NULL, ''),
(36, 1, 'Resource Report', '/admin/reports/resource', '_self', 'voyager-gift', '#000000', 34, 2, '2017-12-20 17:51:57', '2017-12-20 17:52:11', NULL, ''),
(37, 1, 'Product Perception Report', '/admin/reports/productperception', '_self', 'voyager-bag', '#000000', 34, 3, '2017-12-21 13:04:37', '2017-12-21 13:05:51', NULL, ''),
(38, 1, 'Product Prescription Report', '/admin/reports/productprescription', '_self', 'voyager-book-download', '#000000', 34, 4, '2017-12-21 13:05:36', '2017-12-21 13:08:33', NULL, ''),
(39, 1, 'Doctor Status', '/admin/reports/doctorstatus', '_self', 'voyager-activity', '#000000', 34, 5, '2017-12-21 15:59:05', '2017-12-21 15:59:48', NULL, ''),
(40, 1, 'Doctor Report', '/admin/reports/doctor', '_self', 'voyager-barbeque', '#000000', 34, 6, '2017-12-21 15:59:40', '2017-12-21 15:59:50', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_add_permission_group_id_to_permissions_table', 1),
(17, '2017_01_15_000000_create_permission_groups_table', 1),
(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(20, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(21, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(22, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(23, '2017_08_05_000000_add_group_to_settings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pdps`
--

CREATE TABLE `pdps` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `perception_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `potential_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pdps`
--

INSERT INTO `pdps` (`id`, `doctor_id`, `perception_id`, `product_id`, `potential_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 3, '2017-11-26 15:07:15', '2017-12-21 06:03:12'),
(2, 2, 1, 1, 1, '2017-11-26 15:07:15', '2017-12-21 06:03:29'),
(3, 3, 4, 1, 2, '2017-11-26 15:07:15', '2017-11-26 15:07:15'),
(4, 4, 3, 1, 3, '2017-11-26 15:14:35', '2017-12-21 06:02:57'),
(5, 1, 4, 2, 2, '2017-11-26 15:15:47', '2017-11-26 15:15:47'),
(6, 2, 4, 2, 1, '2017-11-26 15:15:47', '2017-11-26 15:15:47'),
(7, 3, 4, 2, 2, '2017-11-26 15:15:47', '2017-11-26 15:15:47'),
(8, 4, 4, 2, 2, '2017-11-26 15:15:47', '2017-11-26 15:15:47'),
(9, 5, 1, 1, 4, '2017-12-16 10:11:14', '2017-12-21 06:01:58'),
(10, 5, 4, 2, 4, '2017-12-16 10:11:14', '2017-12-16 10:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `perceptions`
--

CREATE TABLE `perceptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `perceptions`
--

INSERT INTO `perceptions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Strongly Positive', '2017-11-13 17:51:35', '2017-11-13 17:51:35'),
(2, 'Strongly Negative', '2017-11-13 17:51:45', '2017-11-13 17:51:45'),
(3, 'Weak', '2017-11-13 17:51:53', '2017-11-13 17:51:53'),
(4, 'Unaware', '2017-11-26 11:37:43', '2017-11-26 11:37:43');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`) VALUES
(1, 'browse_admin', NULL, '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(2, 'browse_database', NULL, '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(3, 'browse_media', NULL, '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(4, 'browse_compass', NULL, '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(5, 'browse_menus', 'menus', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(6, 'read_menus', 'menus', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(7, 'edit_menus', 'menus', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(8, 'add_menus', 'menus', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(9, 'delete_menus', 'menus', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(15, 'browse_roles', 'roles', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(16, 'read_roles', 'roles', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(17, 'edit_roles', 'roles', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(18, 'add_roles', 'roles', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(19, 'delete_roles', 'roles', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(20, 'browse_users', 'users', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(21, 'read_users', 'users', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(22, 'edit_users', 'users', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(23, 'add_users', 'users', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(24, 'delete_users', 'users', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(35, 'browse_settings', 'settings', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(36, 'read_settings', 'settings', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(37, 'edit_settings', 'settings', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(38, 'add_settings', 'settings', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(39, 'delete_settings', 'settings', '2017-11-13 17:40:47', '2017-11-13 17:40:47', NULL),
(40, 'browse_designations', 'designations', '2017-11-13 17:49:30', '2017-11-13 17:49:30', NULL),
(41, 'read_designations', 'designations', '2017-11-13 17:49:30', '2017-11-13 17:49:30', NULL),
(42, 'edit_designations', 'designations', '2017-11-13 17:49:30', '2017-11-13 17:49:30', NULL),
(43, 'add_designations', 'designations', '2017-11-13 17:49:30', '2017-11-13 17:49:30', NULL),
(44, 'delete_designations', 'designations', '2017-11-13 17:49:30', '2017-11-13 17:49:30', NULL),
(45, 'browse_perceptions', 'perceptions', '2017-11-13 17:51:17', '2017-11-13 17:51:17', NULL),
(46, 'read_perceptions', 'perceptions', '2017-11-13 17:51:17', '2017-11-13 17:51:17', NULL),
(47, 'edit_perceptions', 'perceptions', '2017-11-13 17:51:17', '2017-11-13 17:51:17', NULL),
(48, 'add_perceptions', 'perceptions', '2017-11-13 17:51:17', '2017-11-13 17:51:17', NULL),
(49, 'delete_perceptions', 'perceptions', '2017-11-13 17:51:17', '2017-11-13 17:51:17', NULL),
(55, 'browse_potentials', 'potentials', '2017-11-16 14:19:56', '2017-11-16 14:19:56', NULL),
(56, 'read_potentials', 'potentials', '2017-11-16 14:19:56', '2017-11-16 14:19:56', NULL),
(57, 'edit_potentials', 'potentials', '2017-11-16 14:19:56', '2017-11-16 14:19:56', NULL),
(58, 'add_potentials', 'potentials', '2017-11-16 14:19:56', '2017-11-16 14:19:56', NULL),
(59, 'delete_potentials', 'potentials', '2017-11-16 14:19:56', '2017-11-16 14:19:56', NULL),
(65, 'browse_call_priorities', 'call_priorities', '2017-11-16 14:22:28', '2017-11-16 14:22:28', NULL),
(66, 'read_call_priorities', 'call_priorities', '2017-11-16 14:22:28', '2017-11-16 14:22:28', NULL),
(67, 'edit_call_priorities', 'call_priorities', '2017-11-16 14:22:28', '2017-11-16 14:22:28', NULL),
(68, 'add_call_priorities', 'call_priorities', '2017-11-16 14:22:28', '2017-11-16 14:22:28', NULL),
(69, 'delete_call_priorities', 'call_priorities', '2017-11-16 14:22:28', '2017-11-16 14:22:28', NULL),
(70, 'browse_products', 'products', '2017-11-16 14:28:18', '2017-11-16 14:28:18', NULL),
(71, 'read_products', 'products', '2017-11-16 14:28:18', '2017-11-16 14:28:18', NULL),
(72, 'edit_products', 'products', '2017-11-16 14:28:18', '2017-11-16 14:28:18', NULL),
(73, 'add_products', 'products', '2017-11-16 14:28:18', '2017-11-16 14:28:18', NULL),
(74, 'delete_products', 'products', '2017-11-16 14:28:18', '2017-11-16 14:28:18', NULL),
(75, 'browse_specialities', 'specialities', '2017-11-16 14:33:23', '2017-11-16 14:33:23', NULL),
(76, 'read_specialities', 'specialities', '2017-11-16 14:33:23', '2017-11-16 14:33:23', NULL),
(77, 'edit_specialities', 'specialities', '2017-11-16 14:33:23', '2017-11-16 14:33:23', NULL),
(78, 'add_specialities', 'specialities', '2017-11-16 14:33:23', '2017-11-16 14:33:23', NULL),
(79, 'delete_specialities', 'specialities', '2017-11-16 14:33:23', '2017-11-16 14:33:23', NULL),
(80, 'browse_profiles', 'profiles', '2017-11-18 15:52:02', '2017-11-18 15:52:02', NULL),
(81, 'read_profiles', 'profiles', '2017-11-18 15:52:02', '2017-11-18 15:52:02', NULL),
(82, 'edit_profiles', 'profiles', '2017-11-18 15:52:02', '2017-11-18 15:52:02', NULL),
(83, 'add_profiles', 'profiles', '2017-11-18 15:52:02', '2017-11-18 15:52:02', NULL),
(84, 'delete_profiles', 'profiles', '2017-11-18 15:52:02', '2017-11-18 15:52:02', NULL),
(85, 'browse_doctors', 'doctors', '2017-11-18 15:57:43', '2017-11-18 15:57:43', NULL),
(86, 'read_doctors', 'doctors', '2017-11-18 15:57:43', '2017-11-18 15:57:43', NULL),
(87, 'edit_doctors', 'doctors', '2017-11-18 15:57:43', '2017-11-18 15:57:43', NULL),
(88, 'add_doctors', 'doctors', '2017-11-18 15:57:43', '2017-11-18 15:57:43', NULL),
(89, 'delete_doctors', 'doctors', '2017-11-18 15:57:43', '2017-11-18 15:57:43', NULL),
(90, 'browse_regions', 'regions', '2017-11-18 16:37:08', '2017-11-18 16:37:08', NULL),
(91, 'read_regions', 'regions', '2017-11-18 16:37:08', '2017-11-18 16:37:08', NULL),
(92, 'edit_regions', 'regions', '2017-11-18 16:37:08', '2017-11-18 16:37:08', NULL),
(93, 'add_regions', 'regions', '2017-11-18 16:37:08', '2017-11-18 16:37:08', NULL),
(94, 'delete_regions', 'regions', '2017-11-18 16:37:08', '2017-11-18 16:37:08', NULL),
(105, 'browse_pdps', 'pdps', '2017-11-26 11:29:15', '2017-11-26 11:29:15', NULL),
(106, 'read_pdps', 'pdps', '2017-11-26 11:29:15', '2017-11-26 11:29:15', NULL),
(107, 'edit_pdps', 'pdps', '2017-11-26 11:29:15', '2017-11-26 11:29:15', NULL),
(108, 'add_pdps', 'pdps', '2017-11-26 11:29:15', '2017-11-26 11:29:15', NULL),
(109, 'delete_pdps', 'pdps', '2017-11-26 11:29:15', '2017-11-26 11:29:15', NULL),
(110, 'browse_product_potentials', 'product_potentials', '2017-11-26 11:43:22', '2017-11-26 11:43:22', NULL),
(111, 'read_product_potentials', 'product_potentials', '2017-11-26 11:43:22', '2017-11-26 11:43:22', NULL),
(112, 'edit_product_potentials', 'product_potentials', '2017-11-26 11:43:22', '2017-11-26 11:43:22', NULL),
(113, 'add_product_potentials', 'product_potentials', '2017-11-26 11:43:22', '2017-11-26 11:43:22', NULL),
(114, 'delete_product_potentials', 'product_potentials', '2017-11-26 11:43:22', '2017-11-26 11:43:22', NULL),
(115, 'browse_schedules', 'schedules', '2017-11-29 14:50:17', '2017-11-29 14:50:17', NULL),
(116, 'read_schedules', 'schedules', '2017-11-29 14:50:17', '2017-11-29 14:50:17', NULL),
(117, 'edit_schedules', 'schedules', '2017-11-29 14:50:17', '2017-11-29 14:50:17', NULL),
(118, 'add_schedules', 'schedules', '2017-11-29 14:50:17', '2017-11-29 14:50:17', NULL),
(119, 'delete_schedules', 'schedules', '2017-11-29 14:50:17', '2017-11-29 14:50:17', NULL),
(120, 'browse_groups', 'groups', '2017-12-16 15:40:37', '2017-12-16 15:40:37', NULL),
(121, 'read_groups', 'groups', '2017-12-16 15:40:37', '2017-12-16 15:40:37', NULL),
(122, 'edit_groups', 'groups', '2017-12-16 15:40:37', '2017-12-16 15:40:37', NULL),
(123, 'add_groups', 'groups', '2017-12-16 15:40:37', '2017-12-16 15:40:37', NULL),
(124, 'delete_groups', 'groups', '2017-12-16 15:40:37', '2017-12-16 15:40:37', NULL),
(130, 'browse_reportsschedule', 'schedules', '2017-12-20 00:00:00', '2017-12-20 00:00:00', NULL),
(131, 'browse_reportsresource', 'schedules', '2017-12-20 00:00:00', '2017-12-20 00:00:00', NULL),
(132, 'browse_reportsproductperception', 'schedules', '2017-12-21 00:00:00', '2017-12-21 00:00:00', NULL),
(133, 'browse_reportsproductprescription', 'schedules', '2017-12-21 00:00:00', '2017-12-21 00:00:00', NULL),
(134, 'browse_reportsdoctor', 'schedules', NULL, NULL, NULL),
(135, 'browse_reportsdoctorstatus', 'schedules', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

CREATE TABLE `permission_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(20, 2),
(21, 1),
(21, 2),
(22, 1),
(22, 2),
(23, 1),
(23, 2),
(24, 1),
(24, 2),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(40, 2),
(40, 3),
(41, 1),
(41, 2),
(41, 3),
(42, 1),
(42, 2),
(42, 3),
(43, 1),
(43, 2),
(43, 3),
(44, 1),
(44, 2),
(44, 3),
(45, 1),
(45, 2),
(45, 3),
(46, 1),
(46, 2),
(46, 3),
(47, 1),
(47, 2),
(47, 3),
(48, 1),
(48, 2),
(48, 3),
(49, 1),
(49, 2),
(49, 3),
(55, 1),
(55, 2),
(55, 3),
(56, 1),
(56, 2),
(56, 3),
(57, 1),
(57, 2),
(57, 3),
(58, 1),
(58, 2),
(58, 3),
(59, 1),
(59, 2),
(59, 3),
(65, 1),
(65, 2),
(65, 3),
(66, 1),
(66, 2),
(66, 3),
(67, 1),
(67, 2),
(67, 3),
(68, 1),
(68, 2),
(68, 3),
(69, 1),
(69, 2),
(69, 3),
(70, 1),
(70, 2),
(70, 3),
(71, 1),
(71, 2),
(71, 3),
(72, 1),
(72, 2),
(72, 3),
(73, 1),
(73, 2),
(73, 3),
(74, 1),
(74, 2),
(74, 3),
(75, 1),
(75, 2),
(75, 3),
(76, 1),
(76, 2),
(76, 3),
(77, 1),
(77, 2),
(77, 3),
(78, 1),
(78, 2),
(78, 3),
(79, 1),
(79, 2),
(79, 3),
(80, 1),
(80, 2),
(80, 3),
(81, 1),
(81, 2),
(81, 3),
(82, 1),
(82, 2),
(82, 3),
(83, 1),
(83, 2),
(83, 3),
(84, 1),
(84, 2),
(84, 3),
(85, 1),
(85, 2),
(85, 3),
(86, 1),
(86, 2),
(86, 3),
(87, 1),
(87, 2),
(87, 3),
(88, 1),
(88, 2),
(88, 3),
(89, 1),
(89, 2),
(89, 3),
(90, 1),
(90, 2),
(90, 3),
(91, 1),
(91, 2),
(91, 3),
(92, 1),
(92, 2),
(92, 3),
(93, 1),
(93, 2),
(93, 3),
(94, 1),
(94, 2),
(94, 3),
(105, 1),
(105, 2),
(105, 3),
(106, 1),
(106, 2),
(106, 3),
(107, 1),
(107, 2),
(107, 3),
(108, 1),
(108, 2),
(108, 3),
(109, 1),
(109, 2),
(109, 3),
(110, 1),
(110, 2),
(110, 3),
(111, 1),
(111, 2),
(111, 3),
(112, 1),
(112, 2),
(112, 3),
(113, 1),
(113, 2),
(113, 3),
(114, 1),
(114, 2),
(114, 3),
(115, 1),
(115, 2),
(116, 1),
(116, 2),
(117, 1),
(117, 2),
(118, 1),
(118, 2),
(119, 1),
(119, 2),
(120, 1),
(120, 2),
(121, 1),
(121, 2),
(122, 1),
(122, 2),
(123, 1),
(123, 2),
(124, 1),
(124, 2),
(130, 1),
(130, 3),
(131, 1),
(131, 3),
(132, 1),
(133, 1),
(134, 1),
(135, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `potentials`
--

CREATE TABLE `potentials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `potentials`
--

INSERT INTO `potentials` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'High', '2017-11-16 15:24:54', '2017-11-16 15:24:54'),
(2, 'Low', '2017-11-16 15:25:01', '2017-11-16 15:25:01'),
(3, 'Medium', '2017-11-16 15:25:23', '2017-11-16 15:25:23');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Kidlac', '2017-11-26 15:07:15', '2017-11-26 15:07:15'),
(2, 'Depain', '2017-11-26 15:15:47', '2017-11-26 15:15:47');

--
-- Triggers `products`
--
DELIMITER $$
CREATE TRIGGER `OnProductInsert` AFTER INSERT ON `products` FOR EACH ROW BEGIN
  DECLARE v_product_id, v_perception_id, v_product_potential_id INT;
  SET v_product_id = NEW.id;
  SET v_perception_id = (SELECT id FROM perceptions WHERE name = 'Unaware');
  SET v_product_potential_id = (SELECT id FROM product_potentials WHERE name = 'NP (Non Prescriber)');  
  
  INSERT INTO pdps (doctor_id, perception_id, potential_id, product_id, created_at, updated_at) 
  	SELECT id, v_perception_id, v_product_potential_id, v_product_id, NOW(), NOW() FROM doctors;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `product_potentials`
--

CREATE TABLE `product_potentials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_potentials`
--

INSERT INTO `product_potentials` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'PC (Product Champion)', '2017-11-26 11:44:42', '2017-11-26 11:44:42'),
(2, 'PR (Regular Prescriber)', '2017-11-26 11:44:56', '2017-11-26 11:44:56'),
(3, 'TR (Trial Prescriber)', '2017-11-26 11:45:11', '2017-11-26 11:45:11'),
(4, 'NP (Non Prescriber)', '2017-11-26 11:45:24', '2017-11-26 11:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'A (Analytics)', '2017-11-18 15:53:28', '2017-11-18 15:53:28'),
(2, 'C (Controller)', '2017-11-18 15:53:47', '2017-11-18 15:53:47'),
(3, 'P (Promoter)', '2017-11-18 15:54:06', '2017-11-18 15:54:06'),
(4, 'R (Relationship)', '2017-11-18 15:54:23', '2017-11-18 15:54:23'),
(5, 'Unaware', '2017-11-18 15:54:40', '2017-11-18 15:54:40');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'YGN', '2017-11-18 16:39:37', '2017-11-18 16:39:37'),
(2, 'MDY', '2017-11-18 16:39:45', '2017-11-18 16:39:45');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Administrator', '2017-11-13 17:40:47', '2017-11-25 14:11:31'),
(2, 'MR', 'MR', '2017-11-13 17:40:47', '2017-11-25 13:48:40'),
(3, 'Manager', 'Manager', '2017-11-25 14:13:53', '2017-11-25 14:13:53');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `pdp_id` int(11) NOT NULL,
  `mr_id` int(11) NOT NULL,
  `other_investment` int(11) DEFAULT '0',
  `sample_qty` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `schedule_date` date DEFAULT NULL,
  `call_priority_id` int(11) DEFAULT NULL,
  `latlong` tinytext COLLATE utf8_unicode_ci,
  `status` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `pdp_id`, `mr_id`, `other_investment`, `sample_qty`, `created_at`, `updated_at`, `schedule_date`, `call_priority_id`, `latlong`, `status`) VALUES
(1, 2, 3, 140000, 10, '2017-12-12 15:22:27', '2017-12-21 13:25:03', '2017-12-23', 2, NULL, 0),
(2, 1, 3, 200000, 100, '2017-12-17 12:25:10', '2017-12-21 13:24:47', '2017-12-22', 1, NULL, 0),
(3, 2, 3, 1500000, 100, '2017-12-20 13:57:42', '2017-12-21 13:22:22', '2017-12-22', 2, '16.80667737,96.14848137', 1),
(4, 1, 4, 1000000, 200, '2017-12-20 13:59:05', '2017-12-21 13:21:05', '2017-12-23', 1, '16.82006971,96.1841011', 1),
(5, 6, 3, 0, 120, '2017-12-20 13:59:41', '2017-12-21 13:20:32', '2017-12-22', 1, '16.8481659,96.22641563', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `specialities`
--

CREATE TABLE `specialities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `specialities`
--

INSERT INTO `specialities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Cardiac Surgeon', '2017-11-16 15:26:15', '2017-11-16 15:26:15'),
(2, 'Cardiologist', '2017-11-16 15:26:40', '2017-11-16 15:26:40'),
(3, 'Dentist', '2017-11-16 15:26:53', '2017-11-16 15:26:53'),
(4, 'Dermatologist', '2017-11-16 15:27:06', '2017-11-16 15:27:06'),
(5, 'Pediatrician', '2017-11-26 15:10:59', '2017-11-26 15:10:59');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `mr_group` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`, `region_id`, `mr_group`) VALUES
(1, 1, 'Admin', 'admin@callplan.com', 'users/default.png', '$2y$10$u9vE3LFA5QZiKiSc/fEg.eynthzh2oZXUiZH.pKi68k3l7032D062', 'TFotYESrC5bLdFPsNX0RO0qe6uLsunEwDxSQeJcQYvmIIqsVokRilJR9CpSY', '2017-11-13 17:42:19', '2017-12-20 17:28:38', 1, 1),
(2, 3, 'marketing manager', 'manager1@callplan.com', 'users/December2017/FZIoVeEIJ61c0btrQj4H.png', '$2y$10$N6qhwSUzGjc5yJZXNAtZmuiUBy/8S9wgDjbFRGRlDvaYTRzoDIzDG', 'cm6pyeWm9cOiutJ49PLc0xp51OTIGBHFWp8SqU1DFGuu5CaIvwMYLNA1MF8Y', '2017-11-25 14:19:03', '2017-12-16 16:53:00', 1, 1),
(3, 2, 'mr1', 'mr1@callplan.com', 'users/December2017/I7IB58WBOlD13Ecg8eY7.png', '$2y$10$ruH.1NNvyJK6PqiDRIrOV.qScAlHrH2jB88cN6gAZ0fNztWgnX/ny', NULL, '2017-12-16 16:41:08', '2017-12-16 16:42:22', 1, 1),
(4, 1, 'mr2', 'mr2@callplan.com', 'users/default.png', '$2y$10$ob.WIBNjetUt.3XZSaSbt.Fyn4lEikqUYt9MPkp.Woly9tZxyedc6', NULL, '2017-12-16 16:50:14', '2017-12-16 16:50:14', 1, 1),
(5, 1, 'Marketing Manager 2', 'manager2@callplan.com', 'users/default.png', '$2y$10$aLbwBMfKMCskKWF1n1r2GO8ZDCAKPuqCtDqHztaYeQ.9TT6VnfTAm', NULL, '2017-12-16 16:53:34', '2017-12-16 16:53:34', 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `call_priorities`
--
ALTER TABLE `call_priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pdps`
--
ALTER TABLE `pdps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perceptions`
--
ALTER TABLE `perceptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permission_groups_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `potentials`
--
ALTER TABLE `potentials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_potentials`
--
ALTER TABLE `product_potentials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `specialities`
--
ALTER TABLE `specialities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `call_priorities`
--
ALTER TABLE `call_priorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pdps`
--
ALTER TABLE `pdps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `perceptions`
--
ALTER TABLE `perceptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `potentials`
--
ALTER TABLE `potentials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_potentials`
--
ALTER TABLE `product_potentials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specialities`
--
ALTER TABLE `specialities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
