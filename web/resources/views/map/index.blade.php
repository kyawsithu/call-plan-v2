@extends('voyager::master')
<title>View on Map</title>
@section('content')
    <style>
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    tfoot {
        display: table-header-group;
    }

    </style>
    <div class="page-content">
        @include('voyager::alerts')
        <div class="panel panel-bordered">
            <div class="panel-body">
                <div style="width: 100%; height: 550px;">
                <?php 
                    Mapper::map($arr[0], $arr[1], [
                        'zoom' => 15, 
                        'center' => true, 
                        'markers' => [
                            'title' => 'My Location', 
                            'animation' => 'DROP'
                        ], 
                        'overlay' => 'TRAFFIC']); 
                ?>
    	            {!! Mapper::render() !!}
                </div>
            </div>
        </div>    
    </div>
@stop

@section('javascript')    
    
@stop
