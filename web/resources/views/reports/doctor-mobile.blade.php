@extends('voyager::mobile')
@section('page_title')
    Doctor Status Report
@stop
@section('content')
    
    <div class="page-content">
        
                <div>
                    <form class="form-inline" method='get' action="/admin/mreports/doctor">
                        <table class="table">
                            
                            <tr>
                                <td><label for="call">Doctor:</label></td>
                                <td>
                                        <input type="input" id="doctor" autocomplete="off"/>
                                        <input type="hidden" id="hiddenDoctorId" name="doctor" />
                                    <?php //echo Form::select('doctor', $doctor, null, ['placeholder' => 'All Doctors', "class" => "form-control"]); ?>
                                </td>
                            </tr>
                            
                            <tr>
                                <td></td>
                                <td><button type="submit" class="btn btn-primary">Search</button></td>
                            </tr>
                        </table>

                        
                        {!! Form::hidden('mr_group', null) !!}
                        
                    </form>
                </div>
                
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Sum of Sample</th>
                        <th>Sum of Other Investment</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $samplegrand = $otherinvestgrand = $count = 0;
                        if (count($investment) > 0) {
                            foreach($investment as $item){  
                                echo "<tr>";
                                    echo "<td>".$item->product."</td>";
                                    echo "<td>".number_format($item->sample_qty, 0, '.', ',')."</td>";
                                    echo "<td>".number_format($item->other_investment, 0, ".", ",")."</td>";
                                echo "</tr>";
                                $samplegrand += $item->sample_qty;
                                $otherinvestgrand += $item->other_investment;
                                $count++;

                            }                                    
                        }
                    ?>
                    <tr>
                        <td>Total :</td>
                        <td>{{number_format($samplegrand, 0, '.', ',')}}</td>
                        <td>{{number_format($otherinvestgrand, 0, '.', ',')}}</td>
                    </tr>
                </tbody>
            </table>
    

    </div>
@stop

@section('javascript')    
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
	</script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap3-typeahead.min.js')}}"></script>
    <script>
    var j = jQuery.noConflict();

    j(document).ready(function() {    
        // DataTable
        var table = j('#example').DataTable({
            "searching": false,
            "lengthChange": false
        });

        var sibs = j('#example').parent().parent().siblings();
        sibs[0].remove();
    } );
    j('#doctor').typeahead({
        source: function(query, process) {
            objects = [];
            map = {};
            var data = <?php echo $doctor; ?>;

            j.each(data, function(i, object) {
                map[object.name] = object;
                objects.push(object.name);
            });
            process(objects);
        },
        updater: function(item) {
            j('#hiddenDoctorId').val(map[item].id);
            return item;
        }
    }); 
    </script>
@stop
