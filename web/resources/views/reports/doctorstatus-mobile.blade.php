@extends('voyager::mobile')
@section('page_title')
    Doctor Status Report
@stop
@section('content')
    
    <div class="page-content">
        
        <div>
            <form class="form-inline" method='get' action="/admin/mreports/doctorstatus">
                <table class="table">
                    <tr>
                        <td><label for="call">Doctor:</label></td>
                        <td>
                            <input type="input" id="doctor" autocomplete="off"/>
                            <input type="hidden" id="hiddenDoctorId" name="doctor" />
                            <?php //echo Form::select('doctor', $doctor, null, ['placeholder' => 'All Doctors', "class" => "form-control"]); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="call">Product:</label></td>
                        <td>
                            <input type="input" id="product" autocomplete="off"/>
                            <input type="hidden" id="hiddenProductId" name="product" />
                            <?php //echo Form::select('product', $product, null, ['placeholder' => 'All Products', "class" => "form-control"]); ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="submit" class="btn btn-primary">Search</button></td>
                    </tr>
                </table>

                {!! Form::hidden('mr_group', null) !!}
            </form>
        </div>        
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Potential</th>
                    <th>Perception</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if (count($data) > 0) {
                        foreach($data as $item){  
                            echo "<tr>";
                                echo "<td>".$item->potential."</td>";
                                echo "<td>".$item->perception."</td>";
                            echo "</tr>";
                        }                                    
                    }
                ?>
            </tbody>
        </table>
            

    </div>
@stop

@section('javascript')    
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
	</script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap3-typeahead.min.js')}}"></script>
    <script>
    var j = jQuery.noConflict();

    j(document).ready(function() {    
        // DataTable
        var table = j('#example').DataTable({
            "searching": false,
            "lengthChange": false
        });
        var sibs = j('#example').parent().parent().siblings();
        sibs[0].remove();
    } );

    // Typeahead

    j('#doctor').typeahead({
        source: function(query, process) {
            objects = [];
            map = {};
            var data = <?php echo $doctor; ?>;

            j.each(data, function(i, object) {
                map[object.name] = object;
                objects.push(object.name);
            });
            process(objects);
        },
        updater: function(item) {
            j('#hiddenDoctorId').val(map[item].id);
            return item;
        }
    }); 

    j('#product').typeahead({
        source: function(query, process) {
            objects = [];
            map = {};
            var data = <?php echo $product; ?>;

            j.each(data, function(i, object) {
                map[object.name] = object;
                objects.push(object.name);
            });
            process(objects);
        },
        updater: function(item) {
            j('#hiddenProductId').val(map[item].id);
            return item;
        }
    }); 
    </script>
@stop
