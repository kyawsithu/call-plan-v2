@extends('voyager::mobile')
@section('page_title')
    Doctor Status Report
@stop
@section('content')
    
    <div class="page-content">
        <form class="form-inline" method='get' action="/admin/mreports/productprescription">
            
            {!! Form::hidden('mr_group', null) !!}
            
            <table class="table">
                <tr>
                    <td><label for="call">Speciality:</label></td>
                    <td><?php echo Form::select('speciality', $speciality, null, ['placeholder' => 'All Specialities', "class" => "form-control"]); ?></td>
                </tr>
                <tr>
                    <td><label for="call">Product:</label></td>
                    <td>
                        <input type="input" id="product" autocomplete="off"/>
                        <input type="hidden" id="hiddenProductId" name="product" />
                        <?php //echo Form::select('product', $product, null, ['placeholder' => 'All Products', "class" => "form-control"]); ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="submit" class="btn btn-primary">Search</button></td>
                </tr>
            </table>
        </form>
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Product Prescription</th>
                    <th>Count</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $count = 0;
                    if (count($data) > 0) {
                        foreach($data as $item){  
                            echo "<tr>";
                                echo "<td>".$item->product_potential."</td>";
                                echo "<td>".number_format($item->total, 0, '.', ',')."</td>";
                            echo "</tr>";
                            $count++;
                        }                                    
                    }
                ?>
            </tbody>
        </table>
    </div>
@stop

@section('javascript')    
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
	</script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap3-typeahead.min.js')}}"></script>

    <script>
    var j = jQuery.noConflict();

    j(document).ready(function() {    
        // DataTable
        var table = j('#example').DataTable({
            "searching": false,
            "lengthChange": false
        });

        var sibs = j('#example').parent().parent().siblings();
        sibs[0].remove();
    } );

    j('#product').typeahead({
        source: function(query, process) {
            objects = [];
            map = {};
            var data = <?php echo $product; ?>;

            j.each(data, function(i, object) {
                map[object.name] = object;
                objects.push(object.name);
            });
            process(objects);
        },
        updater: function(item) {
            j('#hiddenProductId').val(map[item].id);
            return item;
        }
    }); 
    </script>
@stop
