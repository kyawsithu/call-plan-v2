@extends('voyager::mobile')
@section('page_title')
    Schedule Report
@stop
@section('content')
    
    <div class="page-content">
        <form class="form-inline" method='get' action="/admin/mreports/schedule">
            <table class="table">
                <tr>
                    <td><label for="date">Date:</label></td>
                    <td><input type="text" name="date" id="date"></td>
                </tr>
                <tr>
                    <td><label for="call">Status:</label></td>
                    <td><?php echo Form::select('callStatus', $callStatus, null, ['placeholder' => 'All Status', "class" => "form-control"]); ?></td>
                </tr>
                <tr>
                    <td><label for="call">Product:</label></td>
                    <td>
                        <input type="input" id="product" autocomplete="off"/>
                        <input type="hidden" id="hiddenProductId" name="product" />
                        <?php //echo Form::select('product', $product, null, ['placeholder' => 'All Products', "class" => "form-control"]); ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="submit" class="btn btn-primary">Search</button></td>
                </tr>
            </table>
           
            {!! Form::hidden('mr_group', null) !!}
            {!! Form::hidden('mr', null) !!}
            
        </form>
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Calls</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $grand = $count = 0;
                if (count($priority)>0) {
                    foreach($priority as $item){
                        echo "<tr><td><table>";
                        foreach($item as $name=>$col){
                            if ($count != 0) $grand = $grand + $col; 
                            echo "<tr><td>".$name." : ".$col."</td></tr>";
                            $count++;
                        }
                        $count = 0;
                        echo "</table></td></tr>";
                    }
                    echo "<tr><td>Total: ".$grand."</td></tr>";
                }
                
                ?>
            </tbody>
        </table>
    </div>
@stop

@section('javascript')    
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" language="javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/jquery.comiseo.daterangepicker.css') }}">
    <script type="text/javascript" src="{{ asset('js/jquery.comiseo.daterangepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap3-typeahead.min.js')}}"></script>

    <script>
    var j = jQuery.noConflict();

    j(document).ready(function() {    
        // DataTable
        var table = j('#example').DataTable({
            "searching": false,
            "lengthChange": false,
            "sorting": false,
        });

        j("#date").daterangepicker({
            dateFormat: 'yy-mm-dd', 
            rangeSplitter: ' - ',
            setRange: {
                start: moment("{{ $date->start }}", "YYYY-MM-DD").startOf('day').toDate(),
                end: moment("{{ $date->start }}", "YYYY-MM-DD").startOf('day').toDate()
            }
        });
        var sibs = j('#example').parent().parent().siblings();
        sibs[0].remove();

        j('#product').typeahead({
            source: function(query, process) {
                objects = [];
                map = {};
                var data = <?php echo $product; ?>;
    
                j.each(data, function(i, object) {
                    map[object.name] = object;
                    objects.push(object.name);
                });
                process(objects);
            },
            updater: function(item) {
                j('#hiddenProductId').val(map[item].id);
                return item;
            }
        }); 
    } );
    </script>
@stop
