@section('page_title')
    Dashboard Calendar
@stop

@section('content')
<div class="panel panel-bordered">
    <div class="panel-body">
        <div class="container">

            <form class="form-horizontal" method='get' action="/admin">
                <div class="form-group">
                    <div class="col-sm-6">
                        <?php echo Form::select('mr', $mr, null, ['placeholder' => 'All MRs', "class" => "form-control"]); ?>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="table-responsive">
            <!-- Add only div here -->
            <span></span>
            <div id='calendar'></div>        
        </div>
    </div>
</div>
@stop

@section('calendar_script')
<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fullcalendar.min.js')}}"></script>
<script>
var j = jQuery.noConflict();
j(document).ready(function() {
    // page is now ready, initialize the calendar...
    j('#calendar').fullCalendar({
        // put your options and callbacks here
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        defaultDate: new Date(),
        defaultView: 'basicWeek',
        editable: true,
        displayEventTime : false,
        events: <?php echo json_encode($events); ?>,
        eventClick: function(calEvent, jsEvent, view) {;
            if (calEvent.coordinate != 0) {
                window.location.replace("/admin/map/index/" + calEvent.coordinate);
            } else {
                alert("This schedule is not finished yet.");
            }
            
        }
    })
});
</script>
@stop